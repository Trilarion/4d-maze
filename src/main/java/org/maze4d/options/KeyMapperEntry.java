/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

/**
 *
 */
public class KeyMapperEntry {

    public int[] id;
    public boolean down;

    public KeyMapperEntry() {
        id = blank();
        // down is false
    }

    private int[] blank() {
        int[] temp = new int[16];
        for (int i = 0; i < 16; i++) temp[i] = KeyBuffer.ID_NONE;
        return temp;
    }

    public void add(int modifiers, int id) {
        this.id[modifiers] = id;
    }

    private void fill(int[] temp, int modifiers, int id) {
        for (int i = 0; i < 16; i++) {
            temp[i | modifiers] = id;
        }
        // it's true that the loop often writes onto some entries multiple times,
        // but it writes in the correct spots, which is the main thing
    }

    public void fill() {
        int[] temp = blank();
        for (int i = 0; i < 16; i++) {
            if (id[i] != KeyBuffer.ID_NONE) {
                fill(temp, i, id[i]);
            }
        }
        id = temp;

        // how to see that this is correct?
        // if i and j both have ids, and j dominates i,
        // then j has more bits set, is therefore larger,
        // and so gets filled in last, taking precedence

        // if i and j aren't comparable, you get an arbitrary ordering.
        // for example, ALT_MASK is the high bit (8),
        // so Shift+Alt X inherits preferentially from Alt X over Shift X.
    }
}
