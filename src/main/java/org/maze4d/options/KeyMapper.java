/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ui.Key;
import org.maze4d.ui.display.DisplayUtils;

import java.util.HashMap;

/**
 * An object that receives key events and updates a key buffer accordingly.
 */
public class KeyMapper {

    private final HashMap<Integer, KeyMapperEntry> hashMap; // maps key codes into entries
    private final KeyBuffer buf;
    private int modifiersDown;

    public KeyMapper(KeyBuffer buf, OptionsKeys ok, OptionsKeysConfig okc) {
        hashMap = new HashMap<>();
        this.buf = buf;

        setOptions(ok, okc); // covers rest of construction
    }

    private void add(Key key, int id) {
        if (!key.isDefined()) return;

        KeyMapperEntry entry = hashMap.get(key.code);
        if (entry == null) {
            entry = new KeyMapperEntry();
            hashMap.put(key.code, entry);
        }
        entry.add(key.modifiers, id);
    }

    public void setOptions(OptionsKeys ok, OptionsKeysConfig okc) {
        hashMap.clear();
        for (int i = 0; i < OptionsKeys.NKEY; i++) {
            add(ok.key[i], KeyBuffer.getKeyID(i));
        }
        for (int i = 0; i < OptionsKeysConfig.NKEY; i++) {
            add(okc.key[i], KeyBuffer.getKeyConfigID(i));
        }
        for (KeyMapperEntry o : hashMap.values()) {
            o.fill();
        }
        // all new entries start with down set to false
        modifiersDown = 0;
        buf.clearDown(); // in case we remapped a key that was down
    }

    /**
     * Programmatically release all keys that are currently down.
     */
    public void releaseAll() {
        for (KeyMapperEntry o : hashMap.values()) {
            o.down = false;
        }
        modifiersDown = 0;
        buf.clearDown();
    }

    /**
     * Programmatically release a key.<p>
     *
     * This is a function that should only be used under special circumstances.
     */
    public void release(Key key) {
        if (!key.isDefined()) return;

        Object hashKey = key.code;
        KeyMapperEntry entry = hashMap.get(hashKey);
        if (entry == null) return;

        entry.down = false;
        // modifiersDown can stay as it is

        int id = entry.id[modifiersDown];
        if (id != KeyBuffer.ID_NONE) buf.down[id] = false;
    }

    /**
     * Programmatically unrelease a key, so that it is down without having been pressed.<p>
     *
     * This is a function that should only be used under special circumstances.
     * It may be called at most once, immediately after releaseAll has been called.
     */
    public void unrelease(Key key) {
        if (!key.isDefined()) return;

        Object hashKey = key.code;
        KeyMapperEntry entry = hashMap.get(hashKey);
        if (entry == null) return;

        entry.down = true;
        modifiersDown = key.modifiers;

        int id = entry.id[modifiersDown];
        if (id != KeyBuffer.ID_NONE) buf.down[id] = true;
    }

    /**
     * Starting from scratch, figure out which mapped keys are down.
     */
    private void recalculate() {
        buf.clearDown();
        for (KeyMapperEntry entry : hashMap.values()) {
            if (entry.down) {
                int id = entry.id[modifiersDown];
                if (id != KeyBuffer.ID_NONE) {
                    buf.down[id] = true;
                }
            }
        }
    }

    public void keyChanged(int code, boolean down) {

        int modifier = DisplayUtils.translateAllowedModifier(code);
        if (modifier != 0) { // modifier key

            if (down) modifiersDown |= modifier;
            else modifiersDown &= ~modifier;

            recalculate();
            // note that pressing a modifier key doesn't affect buf.pressed

        } else { // regular key

            Object hashKey = code;
            KeyMapperEntry entry = hashMap.get(hashKey);

            if (entry != null) {         // is it mapped?
                if (entry.down != down) { // is it not an auto-repeat?

                    // the condition we are trying to maintain is that for every entry,
                    //
                    //    buf.down[entry.id[modifiersDown]] = entry.down
                    //
                    // (unless the id is ID_NONE, in which case buf.down isn't affected)
                    // here modifiersDown isn't changing, so it's easy,
                    // we just set both down flags to match the one we received

                    entry.down = down;

                    int id = entry.id[modifiersDown];
                    if (id != KeyBuffer.ID_NONE) {
                        if (down) buf.pressed[id] = true;
                        buf.down[id] = down;
                    }
                }
            }
        }
    }

// --- hash table entry class ---

}

