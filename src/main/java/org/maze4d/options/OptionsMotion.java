/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

/**
 * org.maze4d.options.Options for speed and granularity of motion.
 */
public class OptionsMotion implements Validate {

    public double frameRate; // per second
    public double timeMove; // all times in seconds
    public double timeRotate;
    public double timeAlignMove;
    public double timeAlignRotate;


    public void validate() throws ValidationException {

        if (frameRate <= 0) throw App.getException("OptionsMotion.e1");
        if (timeMove <= 0) throw App.getException("OptionsMotion.e2");
        if (timeRotate <= 0) throw App.getException("OptionsMotion.e3");
        if (timeAlignMove <= 0) throw App.getException("OptionsMotion.e4");
        if (timeAlignRotate <= 0) throw App.getException("OptionsMotion.e5");
    }

}

