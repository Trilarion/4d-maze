/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

import java.awt.*;

/**
 * org.maze4d.options.Options for how the walls are colored.
 */
public class OptionsColor implements Validate {

    public static final int COLOR_MODE_EXTERIOR = 0;
    public static final int COLOR_MODE_INTERIOR = 1;
    public static final int COLOR_MODE_BY_ORIENTATION = 2;
    public static final int COLOR_MODE_BY_DIRECTION = 3;

    // --- constants ---
    public static final int NCOLOR_MODE = 4;
    public static final int COLOR_RED = 0;
    public static final int COLOR_GREEN = 1;
    public static final int COLOR_BLUE = 2;
    public static final int COLOR_CYAN = 3;

    // assign numbers to all the colors listed in java.awt.Color (except black)
    // so that we can do array stuff with them
    public static final int COLOR_MAGENTA = 4;
    public static final int COLOR_YELLOW = 5;
    public static final int COLOR_ORANGE = 6;
    public static final int COLOR_PINK = 7;
    public static final int COLOR_DARK_GRAY = 8;
    public static final int COLOR_GRAY = 9;
    public static final int COLOR_LIGHT_GRAY = 10;
    public static final int COLOR_WHITE = 11;
    public static final int NCOLOR = 12;
    private static final Color[] table = {

            Color.red,
            Color.green,
            Color.blue,

            Color.cyan,
            Color.magenta,
            Color.yellow,

            Color.orange,
            Color.pink,

            Color.darkGray,
            Color.gray,
            Color.lightGray,
            Color.white
    };
    private static final int DIM_SAME_MIN = 0;
    private static final int DIM_SAME_MAX = 4;
    public int colorMode;

    // the following table must be kept in sync with the numbers
    public int dimSameParallel;

    public int dimSamePerpendicular;


    public boolean[] enable;

    public OptionsColor() {
        enable = new boolean[NCOLOR];
    }

    public static void copy(OptionsColor dest, OptionsColor src) {
        dest.colorMode = src.colorMode;
        dest.dimSameParallel = src.dimSameParallel;
        dest.dimSamePerpendicular = src.dimSamePerpendicular;
        System.arraycopy(src.enable, 0, dest.enable, 0, NCOLOR);
    }

    public static boolean equals(OptionsColor oc1, OptionsColor oc2) {
        if (oc1.colorMode != oc2.colorMode) return false;
        if (oc1.dimSameParallel != oc2.dimSameParallel) return false;
        if (oc1.dimSamePerpendicular != oc2.dimSamePerpendicular) return false;
        for (int i = 0; i < NCOLOR; i++) if (oc1.enable[i] != oc2.enable[i]) return false;
        return true;
    }


    private int getColorCount() {
        int count = 0;
        for (int i = 0; i < NCOLOR; i++) {
            if (enable[i]) count++;
        }
        return count;
    }

    public Color[] getColors() {
        Color[] color = new Color[getColorCount()];

        int next = 0;
        for (int i = 0; i < NCOLOR; i++) {
            if (enable[i]) {
                color[next] = table[i];
                next++;
            }
        }

        return color;
    }

    public void validate() throws ValidationException {

        if (colorMode != COLOR_MODE_EXTERIOR
                && colorMode != COLOR_MODE_INTERIOR
                && colorMode != COLOR_MODE_BY_ORIENTATION
                && colorMode != COLOR_MODE_BY_DIRECTION) throw App.getException("OptionsColor.e1");

        if (dimSameParallel < DIM_SAME_MIN || dimSameParallel > DIM_SAME_MAX)
            throw App.getException("OptionsColor.e2", new Object[]{DIM_SAME_MIN, DIM_SAME_MAX});
        if (dimSamePerpendicular < DIM_SAME_MIN || dimSamePerpendicular > DIM_SAME_MAX)
            throw App.getException("OptionsColor.e3", new Object[]{DIM_SAME_MIN, DIM_SAME_MAX});

        if (getColorCount() == 0) throw App.getException("OptionsColor.e4");
    }

}
