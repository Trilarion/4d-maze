/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

/**
 * An interface for getting applicable options to modify.
 */
public interface IOptions {

    OptionsColor oc();

    OptionsView ov();

    OptionsStereo os();

}

