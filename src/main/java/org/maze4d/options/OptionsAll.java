/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

/**
 * An object that contains all the option settings that can be edited in the options dialog.<p>
 *
 * Unlike the other options objects, org.maze4d.options.OptionsAll does not automatically create its substructures.
 * They may be null, and they may be shared among themselves or with other objects.
 */
public class OptionsAll {

    public Options opt;
    public OptionsMap omCurrent;
    public OptionsColor ocCurrent;
    public OptionsView ovCurrent; // out of order, but similar to ocCurrent
    public OptionsSeed oeCurrent;
    public OptionsSeed oeNext;

}

