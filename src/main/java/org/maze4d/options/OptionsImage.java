/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

/**
 * org.maze4d.options.Options for image file generation.
 */
public class OptionsImage implements Validate {

    public static final int BACKGROUND_BLACK = 0;
    public static final int BACKGROUND_WHITE = 1;
    public static final int CONVERT_NORMAL = 0;
    public static final int CONVERT_GRAY_SCALE = 1;
    public static final int CONVERT_B_AND_W = 2;

    // --- constants ---
    public int background;
    public boolean invertColors;
    public int convertColors;
    public double lineWidth;
    public double oneInch;



    public static void copy(OptionsImage dest, OptionsImage src) {
        dest.background = src.background;
        dest.invertColors = src.invertColors;
        dest.convertColors = src.convertColors;
        dest.lineWidth = src.lineWidth;
        dest.oneInch = src.oneInch;
    }


    public void validate() throws ValidationException {

        if (background != BACKGROUND_BLACK
                && background != BACKGROUND_WHITE) throw App.getException("OptionsImage.e1");

        if (convertColors != CONVERT_NORMAL
                && convertColors != CONVERT_GRAY_SCALE
                && convertColors != CONVERT_B_AND_W) throw App.getException("OptionsImage.e2");

        if (lineWidth < 0) throw App.getException("OptionsImage.e3");
        if (oneInch <= 0) throw App.getException("OptionsImage.e4");
    }

}

