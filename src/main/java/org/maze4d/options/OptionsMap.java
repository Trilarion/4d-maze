/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

/**
 * org.maze4d.options.Options for the size and shape of the map.
 */
public class OptionsMap implements Validate {

    private static final int DIM_MAP_MIN = 1;
    private static final int SIZE_MIN = 2;
    private static final int SIZE_UNUSED = 1;
    private static final double DENSITY_MIN = 0;
    private static final double DENSITY_MAX = 1;
    private static final double PROBABILITY_MIN = 0;
    private static final double PROBABILITY_MAX = 1;
    public int dimMap;

    public int[] size;


    public double density;

        public double twistProbability;
    public double branchProbability;
    public boolean allowLoops;
    public double loopCrossProbability;
    private int dim;

    public OptionsMap(int dim) {
        this.dim = dim;
        size = new int[dim];
    }

    public static void copy(OptionsMap dest, OptionsMap src) {
        dest.dim = src.dim;
        dest.dimMap = src.dimMap;
        dest.size = src.size.clone(); // can't just copy values, length may be different
        dest.density = src.density;
        dest.twistProbability = src.twistProbability;
        dest.branchProbability = src.branchProbability;
        dest.allowLoops = src.allowLoops;
        dest.loopCrossProbability = src.loopCrossProbability;
    }

    public void validateDimMap() throws ValidationException {
        if (dimMap < DIM_MAP_MIN || dimMap > dim)
            throw App.getException("OptionsMap.e1", new Object[]{DIM_MAP_MIN, dim});
    }

    public void validate() throws ValidationException {

        validateDimMap();

        int i = 0;
        for (; i < dimMap; i++) {
            if (size[i] < SIZE_MIN) throw App.getException("OptionsMap.e2", new Object[]{SIZE_MIN});
        }
        for (; i < dim; i++) {
            if (size[i] != SIZE_UNUSED) throw App.getException("OptionsMap.e7", new Object[]{SIZE_UNUSED});
        }

        if (density < DENSITY_MIN || density > DENSITY_MAX)
            throw App.getException("OptionsMap.e3", new Object[]{DENSITY_MIN, DENSITY_MAX});

        if (twistProbability < PROBABILITY_MIN || twistProbability > PROBABILITY_MAX)
            throw App.getException("OptionsMap.e4", new Object[]{PROBABILITY_MIN, PROBABILITY_MAX});
        if (branchProbability < PROBABILITY_MIN || branchProbability > PROBABILITY_MAX)
            throw App.getException("OptionsMap.e5", new Object[]{PROBABILITY_MIN, PROBABILITY_MAX});
        if (loopCrossProbability < PROBABILITY_MIN || loopCrossProbability > PROBABILITY_MAX)
            throw App.getException("OptionsMap.e6", new Object[]{PROBABILITY_MIN, PROBABILITY_MAX});
    }

}

