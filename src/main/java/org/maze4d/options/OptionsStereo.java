/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

/**
 * org.maze4d.options.Options for how stereo pairs are displayed.
 */
public class OptionsStereo implements Validate {

    private static final double TILT_VERTICAL_MAX = 45;
    private static final double TILT_HORIZONTAL_MAX = 45;
    public boolean enable;
    public double screenWidth;
    public double screenDistance;
    public double eyeSpacing;
    public boolean cross;


    public double tiltVertical;

        public double tiltHorizontal;

    public static void copy(OptionsStereo dest, OptionsStereo src) {
        dest.enable = src.enable;
        dest.screenWidth = src.screenWidth;
        dest.screenDistance = src.screenDistance;
        dest.eyeSpacing = src.eyeSpacing;
        dest.cross = src.cross;
        dest.tiltVertical = src.tiltVertical;
        dest.tiltHorizontal = src.tiltHorizontal;
    }

    public void validate() throws ValidationException {

        if (screenWidth <= 0) throw App.getException("OptionsStereo.e1");
        if (screenDistance <= 0) throw App.getException("OptionsStereo.e2");
        if (eyeSpacing <= 0) throw App.getException("OptionsStereo.e3");

        if (Math.abs(tiltVertical) > TILT_VERTICAL_MAX)
            throw App.getException("OptionsStereo.e4", new Object[]{TILT_VERTICAL_MAX});
        if (Math.abs(tiltHorizontal) > TILT_HORIZONTAL_MAX)
            throw App.getException("OptionsStereo.e5", new Object[]{TILT_HORIZONTAL_MAX});
    }

}

