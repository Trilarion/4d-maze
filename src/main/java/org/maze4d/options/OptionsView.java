/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

/**
 * org.maze4d.options.Options for how the maze is viewed.
 */
public class OptionsView implements Validate {

    public static final int DEPTH_MIN = 0;
    public static final int DEPTH_MAX = 10;
    private static final double SCALE_MIN = 0;
    private static final double SCALE_MAX = 1;

    public int depth;

        public boolean[] texture; // 0 is for cell boundaries, 1-9 for wall texture
    public double retina;
    public double scale;

    public OptionsView() {
        texture = new boolean[10];
    }

    public void validate() throws ValidationException {

        if (depth < DEPTH_MIN || depth > DEPTH_MAX)
            throw App.getException("OptionsView.e1", new Object[]{DEPTH_MIN, DEPTH_MAX});

        if (retina <= 0) throw App.getException("OptionsView.e2");

        if (scale <= SCALE_MIN || scale > SCALE_MAX)
            throw App.getException("OptionsView.e3", new Object[]{SCALE_MIN, SCALE_MAX});
    }

}

