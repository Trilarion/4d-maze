/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.options;

/**
 * A buffer that keeps track of mapped keyboard state.
 */
public class KeyBuffer {

    public static final int NID = OptionsKeys.NKEY + OptionsKeysConfig.NKEY;
    public static final int ID_NONE = -1;

// --- ids ---

    // map org.maze4d.options.OptionsKeys and org.maze4d.options.OptionsKeysConfig numbers into a single ID space
    public final boolean[] pressed;
    public final boolean[] down;

    public KeyBuffer() {
        pressed = new boolean[NID];
        down = new boolean[NID];
    }

    public static int getKeyID(int i) {
        return i;
    }

    public static int getKeyConfigID(int i) {
        return OptionsKeys.NKEY + i;
    }

    public void clearPressed() {
        for (int i = 0; i < NID; i++) pressed[i] = false;
    }

    public void clearDown() {
        for (int i = 0; i < NID; i++) down[i] = false;
    }

}

