/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.property;

import org.maze4d.ValidationException;
import org.maze4d.ui.Validate;
import org.maze4d.utils.App;

import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.Properties;

/**
 * A store, i.e., a place where objects can be stored persistently,
 * implemented on top of a Properties object.<p>
 *
 * The logic for primitive types is a bit convoluted.
 * The end result is that array components and structure fields
 * can be (some) primitive types but not primitive wrapper types.
 * The latter will store correctly, but won't load.
 */
public class PropertyStore implements Store { // TODO use STORABLE interface for objects

    private final Properties p;

    public PropertyStore(Properties p) {
        this.p = p;
    }

    /**
     * Parse a string to produce a boolean value.
     * This is different from the function Boolean.parseBoolean in that
     * (a) it exists
     * and (b) it uses strict parsing, unlike the related functions in Boolean.
     */
    private boolean parseBoolean(String s) throws NumberFormatException {
        if ("true".equals(s)) return true;
        else if ("false".equals(s)) return false;
        else throw new NumberFormatException(); // not really a number, but close enough
    }

    /**
     * Get a property, converting the not-found condition into an exception.
     * In most cases, we could just return the null
     * and let the parse function convert it into a NumberFormatException,
     * but I think the error message is clearer this way.
     */
    private String getProperty(String key) throws ValidationException {
        String value = p.getProperty(key);
        if (value == null) throw App.getException("PropertyStore.e1", new Object[]{key});
        return value;
    }

    private String arrayKey(String key, int i) {
        return key + "[" + i + "]";
    }

    private String fieldKey(String key, java.lang.reflect.Field field) {
        return key + "." + field.getName();
    }

    /**
     * An analogue of Class.isPrimitive for primitive wrapper types.
     */
    private boolean isPrimitiveWrapper(Class c) {

        return (c == Boolean.class
                || c == Integer.class
                || c == Long.class
                || c == Double.class

                || c == Byte.class
                || c == Short.class
                || c == Float.class
                || c == Character.class);
    }

    public boolean getBoolean(String key) throws ValidationException {
        String value = getProperty(key);
        try {
            return parseBoolean(value); // there is no Boolean.parseBoolean, see above
        } catch (NumberFormatException e) {
            throw App.getException("PropertyStore.e2", new Object[]{key, value});
        }
    }

    public int getInteger(String key) throws ValidationException {
        String value = getProperty(key);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw App.getException("PropertyStore.e3", new Object[]{key, value});
        }
    }

    public long getLong(String key) throws ValidationException {
        String value = getProperty(key);
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw App.getException("PropertyStore.e4", new Object[]{key, value});
        }
    }

    public double getDouble(String key) throws ValidationException {
        String value = getProperty(key);
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw App.getException("PropertyStore.e5", new Object[]{key, value});
        }
    }

    public String getString(String key) throws ValidationException {
        return getProperty(key);
    }

    /**
     * Get data from the store into an array.
     * The array and any substructures and subarrays must exist and be correctly sized.
     */
    private void getArray(String key, Object array) throws ValidationException {

        Class componentType = array.getClass().getComponentType();
        boolean isPrimitive = componentType.isPrimitive();
        int len = Array.getLength(array);

        for (int i = 0; i < len; i++) {
            String arrayKey = arrayKey(key, i);

            if (isPrimitive) Array.set(array, i, getPrimitive(arrayKey, componentType));
            else getObject(arrayKey, Array.get(array, i));
        }

        // the Array functions can throw a couple of exceptions,
        // but only if there's programmer error, so don't catch them.
    }

    /**
     * Get data from the store into a structure.
     * The structure and any substructures and subarrays must exist and be correctly sized.
     */
    private void getStruct(String key, Object o) throws ValidationException {

        java.lang.reflect.Field[] field = o.getClass().getFields();

        for (java.lang.reflect.Field value : field) {

            int mods = value.getModifiers();
            if (Modifier.isStatic(mods)
                    || Modifier.isFinal(mods)) continue; // ignore globals and constants

            Class fieldType = value.getType();
            String fieldKey = fieldKey(key, value);

            try {

                if (fieldType.isPrimitive()) value.set(o, getPrimitive(fieldKey, fieldType));
                else getObject(fieldKey, value.get(o));

            } catch (IllegalAccessException e) {
                throw App.getException("PropertyStore.e9", new Object[]{key, e.getMessage()});
            }
        }

        if (o instanceof Validate) ((Validate) o).validate();
    }

    /**
     * Get a (wrapped) object of primitive type from the store.
     */
    private Object getPrimitive(String key, Class c) throws ValidationException {

        if (c == Boolean.TYPE) return getBoolean(key);
        else if (c == Integer.TYPE) return getInteger(key);
        else if (c == Long.TYPE) return getLong(key);
        else if (c == Double.TYPE) return getDouble(key);
        else throw App.getException("PropertyStore.e6", new Object[]{key, c.getName()});
    }

    /**
     * Get data from the store into an object.
     * The object and any substructures and subarrays must exist and be correctly sized.<p>
     *
     * Because the primitive wrapper types aren't mutable, i.e., don't act as pointers,
     * we can't handle primitive types here.  Use getPrimitive instead.
     */
    public void getObject(String key, Object o) throws ValidationException {
        Class c = o.getClass();

        if (c.isPrimitive()) throw App.getException("PropertyStore.e7", new Object[]{key, c.getName()});
        else if (c.isArray()) getArray(key, o);
        else getStruct(key, o);
    }

    public void putBoolean(String key, boolean b) {
        p.setProperty(key, String.valueOf(b));
    }

    public void putInteger(String key, int i) {
        p.setProperty(key, String.valueOf(i));
    }

    public void putLong(String key, long l) {
        p.setProperty(key, String.valueOf(l));
    }

    public void putDouble(String key, double d) {
        p.setProperty(key, String.valueOf(d));
    }

    public void putString(String key, String s) {
        p.setProperty(key, s);
    }

    /**
     * Put an array into the store.
     */
    private void putArray(String key, Object array) throws ValidationException {

        int len = Array.getLength(array);

        for (int i = 0; i < len; i++) {
            putObject(arrayKey(key, i), Array.get(array, i));
        }

        // the Array functions can throw a couple of exceptions,
        // but only if there's programmer error, so don't catch them.
    }

    /**
     * Put a structure into the store.
     */
    private void putStruct(String key, Object o) throws ValidationException {

        java.lang.reflect.Field[] field = o.getClass().getFields();

        for (java.lang.reflect.Field value : field) {

            int mods = value.getModifiers();
            if (Modifier.isStatic(mods)
                    || Modifier.isFinal(mods)) continue; // ignore globals and constants

            try {

                putObject(fieldKey(key, value), value.get(o));

            } catch (IllegalAccessException e) {
                throw App.getException("PropertyStore.e10", new Object[]{key, e.getMessage()});
            }
        }
    }

    /**
     * Put an object into the store.  The object can be a primitive wrapper type.
     */
    public void putObject(String key, Object o) throws ValidationException { // TODO if putObject can be refactored, there would be no need for reflection anymore, just implement explicitely all possible cases?
        Class c = o.getClass();

        if (c == Boolean.class) putBoolean(key, (Boolean) o);
        else if (c == Integer.class) putInteger(key, (Integer) o);
        else if (c == Long.class) putLong(key, (Long) o);
        else if (c == Double.class) putDouble(key, (Double) o);

        else if (isPrimitiveWrapper(c)) throw App.getException("PropertyStore.e8", new Object[]{key, c.getName()});
        else if (c.isArray()) putArray(key, o);
        else putStruct(key, o);
    }

}

