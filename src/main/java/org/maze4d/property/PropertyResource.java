/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.property;

import org.maze4d.ValidationException;
import org.maze4d.utils.App;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A utility class for loading properties from resources.
 * The code is very similar to the code in org.maze4d.property.PropertyFile.
 */
public final class PropertyResource { // TODO unify PropertyFile and PropertyResource

    private PropertyResource() {
    }

    private static void loadProperties(String name, Properties p) throws IOException, ValidationException {
        try (InputStream stream = PropertyResource.class.getClassLoader().getResourceAsStream(name)) {
            if (stream == null) throw App.getException("PropertyResource.e3", new Object[]{name});
            p.load(stream);
        }
    }

    public static void load(String name, Storable storable) throws ValidationException {
        Properties p = new Properties();

        try {
            loadProperties(name, p);
        } catch (IOException e) {
            throw App.getException("PropertyResource.e1", new Object[]{name, e.getMessage()});
        }

        try {
            Store store = new PropertyStore(p);
            storable.load(store);
        } catch (ValidationException e) {
            throw App.getException("PropertyResource.e2", new Object[]{name, e.getMessage()});
        }
    }

}

