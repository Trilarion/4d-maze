/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.property;

import org.maze4d.ValidationException;

/**
 * An interface for objects that know how to load and store themselves.
 */
public interface Storable { // TODO instead of a store, use a PropertyStore instead?

    void load(Store store) throws ValidationException;

    void save(Store store) throws ValidationException;

}

