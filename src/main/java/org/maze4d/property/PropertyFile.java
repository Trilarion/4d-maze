/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.property;

import org.maze4d.ValidationException;
import org.maze4d.utils.App;

import java.io.*;
import java.util.Properties;

/**
 * A utility class for loading and saving property files.<p>
 *
 * When storing, the properties end up in the file in hash table order.
 * That's slightly unfortunate, but not worth correcting.
 */
public final class PropertyFile {
    private PropertyFile() {
    }

// --- stream encapsulation ---

    private static void loadProperties(File file, Properties p) throws IOException {
        try (InputStream stream = new FileInputStream(file)) {
            p.load(stream);
        }

        // note: if we get here because of an exception,
        // and then the close throws an exception,
        // that exception replaces the original one.
        // it does not immediately terminate the program (as in C++).
    }

    private static void storeProperties(File file, Properties p) throws IOException {
        try (OutputStream stream = new FileOutputStream(file)) {
            p.store(stream, null);
        }
    }

    public static void load(File file, Storable storable) throws ValidationException {
        Properties p = new Properties();

        try {
            loadProperties(file, p);
        } catch (IOException e) {
            throw App.getException("PropertyFile.e1", new Object[]{file.getName(), e.getMessage()});
        }

        try {
            Store store = new PropertyStore(p);
            storable.load(store);
        } catch (ValidationException e) {
            throw App.getException("PropertyFile.e2", new Object[]{file.getName(), e.getMessage()});
        }
    }

    public static void save(File file, Storable storable) throws ValidationException {
        Properties p = new Properties();

        try {
            Store store = new PropertyStore(p);
            storable.save(store);
        } catch (ValidationException e) {
            throw App.getException("PropertyFile.e3", new Object[]{file.getName(), e.getMessage()});
        }

        try {
            storeProperties(file, p);
        } catch (IOException e) {
            throw App.getException("PropertyFile.e4", new Object[]{file.getName(), e.getMessage()});
        }
    }

}

