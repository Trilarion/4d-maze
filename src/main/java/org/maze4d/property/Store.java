/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.property;

import org.maze4d.ValidationException;

/**
 * An interface for loading and storing objects.
 * org.maze4d.property.PropertyStore is the only thing that implements it,
 * but I find it provides clarity in the code.
 */
public interface Store {

    boolean getBoolean(String key) throws ValidationException;

    int getInteger(String key) throws ValidationException;

    long getLong(String key) throws ValidationException;

    double getDouble(String key) throws ValidationException;

    String getString(String key) throws ValidationException;

    /**
     * Get data from the store into an object.
     * The object and any substructures and subarrays must exist and be correctly sized.
     */
    void getObject(String key, Object o) throws ValidationException;

    void putBoolean(String key, boolean b);

    void putInteger(String key, int i);

    void putLong(String key, long l);

    void putDouble(String key, double d);

    void putString(String key, String s);

    /**
     * Put an object into the store.
     */
    void putObject(String key, Object o) throws ValidationException;
}
