/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d;

/**
 * An exception subclass used for validation in the user interface and in I/O routines.
 */
public class ValidationException extends Exception {

    private static final long serialVersionUID = 3847454375094795646L;

    public ValidationException(String message) {
        super(message);
    }

}

