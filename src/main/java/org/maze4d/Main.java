/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d;

import org.maze4d.ui.MainFrame;
import org.maze4d.utils.App;

/**
 *
 */
public final class Main {

    private Main() {
    }

    public static void main(String[] args) {

        if (args.length != 0) {
            System.out.println(App.getString("Maze.s13"));
            return;
        }

        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }
}
