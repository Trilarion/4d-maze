/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

public class CommandChangeTexture extends Command {
    private final Controller controller;
    private final int i;

    public CommandChangeTexture(Controller controller, int i) {
        this.controller = controller;
        this.i = i;
    }

    public boolean isOnPress() {
        return true;
    }

    public boolean isExclusive() {
        return false;
    }

    public boolean isExcluded() {
        return false;
    }

    public boolean run() {
        controller.changeTexture(i);
        return false;
    }
}
