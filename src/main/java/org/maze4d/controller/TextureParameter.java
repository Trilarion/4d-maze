/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

import org.maze4d.ui.Validate;

public class TextureParameter extends Parameter {

    private final Controller controller;
    private final int i;

    public TextureParameter(Controller controller, int i) {
        super(controller, 0);
        this.controller = controller;
        this.i = i;
    }

    public Validate getOptions() {
        return controller.options.ov();
    }

    public double get() {
        return controller.options.ov().texture[i] ? 1 : 0;
    }

    public void set(double d) {
        controller.options.ov().texture[i] = (d != 0);
    }

    public double change(double d, int sign) {
        return (d != 0) ? 0 : 1;
    }

    public void apply(double d) {
        controller.engine.setTexture(controller.options.ov().texture);
    }
}
