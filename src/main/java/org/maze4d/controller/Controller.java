/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

import org.maze4d.ValidationException;
import org.maze4d.model.Align;
import org.maze4d.options.*;
import org.maze4d.ui.Engine;
import org.maze4d.ui.IClock;
import org.maze4d.ui.Menu;
import org.maze4d.ui.Validate;

/**
 * An object that watches a key buffer and controls an engine based on which keys are down.
 */
public class Controller implements IClock {

    public final KeyBuffer buf;
    public final IOptions options;
    public final Menu menu;
    public final Engine engine;
    public final int[] param;
    public final Command[] reg1; // temporary register for tick
    public Command[] command;
    public Parameter[] parameter;
    public Parameter[] textureParameter;
    public int nMove;
    public int nRotate;
    public double dMove;
    public double dRotate;
    public double dAlignMove;
    public double dAlignRotate;
    public int dim;
    public boolean alignMode;
    public Command commandActive;
    public int nActive;
    public double dActive;
    public Align alignActive;

    public Controller(KeyBuffer buf, IOptions options, Menu menu, Engine engine) {
        this.buf = buf;
        this.options = options;
        this.menu = menu;
        this.engine = engine;

        constructCommands();
        constructParameters();

        param = new int[6];
        // options fields initialized via setOptions

        // dim and align mode can wait 'til reset

        // active command info starts out null

        reg1 = new Command[KeyBuffer.NID]; // won't use that many in real life
    }

    public void setOptions(OptionsKeysConfig okc, OptionsMotion ot) {

        System.arraycopy(okc.param, 0, param, 0, 6);

        // the frame rate and command times are all positive,
        // so the number of steps will always be at least 1 ...

        nMove = (int) Math.ceil(ot.frameRate * ot.timeMove);
        nRotate = (int) Math.ceil(ot.frameRate * ot.timeRotate);
        // these two only used inside setOptions
        int nAlignMove = (int) Math.ceil(ot.frameRate * ot.timeAlignMove);
        int nAlignRotate = (int) Math.ceil(ot.frameRate * ot.timeAlignRotate);

        // ... therefore, the distances will never exceed 1,
        // and the angles will never exceed 90 degrees

        dMove = 1 / (double) nMove;
        dRotate = 90 / (double) nRotate;
        dAlignMove = 1 / (double) nAlignMove;
        dAlignRotate = 90 / (double) nAlignRotate;
    }

    public boolean getAlignMode() {
        return alignMode;
    }

    /**
     * Immediately stop any operation in progress.<p>
     *
     * This is only called when the game state has been reset (new, load, restart),
     * so you don't need to worry about leaving the engine out of alignment.
     */
    public void reset(int dim, boolean alignMode) {
        this.dim = dim;
        this.alignMode = alignMode;

        buf.clearPressed(); // in case there is a leftover press
        // the key mapper is responsible for the down flags

        commandActive = null;
        alignActive = null;
    }


    public boolean tick() {

        // figure out which commands are being invoked

        int n = 0;
        for (int i = 0; i < KeyBuffer.NID; i++) {
            Command com = command[i];

            if (com.isOnPress()) {
                if (!buf.pressed[i]) continue;
            } else {
                if (!buf.down[i]) continue;
            }

            if (!com.canRun()) continue;

            if (com.isImmediate()) {
                com.run();
                continue; // don't add to list
            }
            // the point here is to prevent menu commands from causing re-render

            if (com.isExclusive()) {
                if (commandActive != null) continue; // already got one, maybe even already running

                commandActive = com;
                com.activate();
                continue; // don't add to list, will handle separately
            }

            reg1[n] = com; // found a nonexclusive command
            n++;
        }

        buf.clearPressed(); // ok, we've looked at that

        // check idleness now, simplifies later code

        if (n == 0 && commandActive == null) return false; // idle

        // run the commands

        for (int i = 0; i < n; i++) {

            if (commandActive != null && reg1[i].isExcluded()) continue;
            // exclusion doesn't alter the idleness result above.
            // if commandActive is null, nothing is excluded,
            // and if it's not, commandActive itself keeps us from becoming idle

            reg1[i].run(); // ignore result, nothing we can do with it
        }

        // we don't need to set reg1 back to null,
        // all it does is point to shared objects that live in the command array

        if (commandActive != null) {
            if (!commandActive.run()) commandActive = null;
        }

        engine.renderAbsolute();
        // if we didn't do anything at all, we would have exited above,
        // so we know that we need to re-render at some level.
        // it's not worth figuring out exactly what level, this is fast enough

        return true;
    }

    // a command is exclusive if it requires exclusive control of the user's position
    //
    // only exclusive commands can become active and request more time;
    // non-exclusive commands must always complete in a single step

    // a command is excluded if it can't run at the same time as an exclusive command.
    //
    // exclusive commands are excluded by definition; for such commands,
    // isExcluded is not called, but should return true for consistency.

    private void constructCommands() {

        command = new Command[KeyBuffer.NID];

        command[KeyBuffer.getKeyID(OptionsKeys.KEY_FORWARD)] = new CommandMove(this, -1, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_BACK)] = new CommandMove(this, -1, -1);

        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_LEFT)] = new CommandRotate(this, -1, 0, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_RIGHT)] = new CommandRotate(this, -1, 0, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_UP)] = new CommandRotate(this, -1, 1, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_DOWN)] = new CommandRotate(this, -1, 1, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_IN)] = new CommandRotate(this, -1, 2, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_TURN_OUT)] = new CommandRotate(this, -1, 2, +1);

        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_LEFT)] = new CommandMove(this, 0, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_RIGHT)] = new CommandMove(this, 0, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_UP)] = new CommandMove(this, 1, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_DOWN)] = new CommandMove(this, 1, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_IN)] = new CommandMove(this, 2, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SLIDE_OUT)] = new CommandMove(this, 2, +1);

        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_UP_LEFT)] = new CommandRotate(this, 0, 1, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_UP_RIGHT)] = new CommandRotate(this, 0, 1, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_IN_LEFT)] = new CommandRotate(this, 2, 0, +1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_IN_RIGHT)] = new CommandRotate(this, 2, 0, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_IN_UP)] = new CommandRotate(this, 2, 1, -1);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_SPIN_IN_DOWN)] = new CommandRotate(this, 2, 1, +1);

        command[KeyBuffer.getKeyID(OptionsKeys.KEY_ALIGN)] = new CommandAlign(this);
        command[KeyBuffer.getKeyID(OptionsKeys.KEY_CHANGE_ALIGN_MODE)] = new CommandChangeAlignMode(this);

        command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_NEW_GAME)] = new CommandMenu(menu::doNew);
        command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_OPTIONS)] = new CommandMenu(menu::doOptions);
        command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_EXIT_GAME)] = new CommandMenu(menu::doExit);

        for (int i = 0; i < 10; i++) {
            command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_TEXTURE + i)] = new CommandChangeTexture(this, i);
        }

        for (int i = 0; i < 6; i++) {
            command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_PARAM_DECREASE + i)] = new CommandChangeParameter(this, i, -1);
            command[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_PARAM_INCREASE + i)] = new CommandChangeParameter(this, i, +1);
        }
    }

    private void constructParameters() {

        parameter = new Parameter[OptionsKeysConfig.NPARAM];

        parameter[OptionsKeysConfig.PARAM_COLOR_MODE] = new Parameter(this, 1) {
            public Validate getOptions() {
                return options.oc();
            }

            public double get() {
                return options.oc().colorMode;
            }

            public void set(double d) {
                options.oc().colorMode = (int) d;
            }

            public double change(double d, int sign) { // cycle
                return (d + sign + OptionsColor.NCOLOR_MODE) % OptionsColor.NCOLOR_MODE;
            }

            public void apply(double d) {
                engine.setColorMode((int) d);
            }
        };

        parameter[OptionsKeysConfig.PARAM_DEPTH] = new Parameter(this, 1) {
            public Validate getOptions() {
                return options.ov();
            }

            public double get() {
                return options.ov().depth;
            }

            public void set(double d) {
                options.ov().depth = (int) d;
            }

            public void apply(double d) {
                engine.setDepth((int) d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_RETINA] = new Parameter(this, 100) {
            public Validate getOptions() {
                return options.ov();
            }

            public double get() {
                return options.ov().retina;
            }

            public void set(double d) {
                options.ov().retina = d;
            }

            public void apply(double d) {
                engine.setRetina(d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_SCALE] = new Parameter(this, 100) {
            public Validate getOptions() {
                return options.ov();
            }

            public double get() {
                return options.ov().scale;
            }

            public void set(double d) {
                options.ov().scale = d;
            }

            public void apply(double d) {
                engine.setScale(d);
            }
        };

        parameter[OptionsKeysConfig.PARAM_SCREEN_WIDTH] = new Parameter(this, 10) {
            public double get() {
                return options.os().screenWidth;
            }

            public void set(double d) {
                options.os().screenWidth = d;
            }

            public void apply(double d) {
                engine.setScreenWidth(d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_SCREEN_DISTANCE] = new Parameter(this, 10) {
            public double get() {
                return options.os().screenDistance;
            }

            public void set(double d) {
                options.os().screenDistance = d;
            }

            public void apply(double d) {
                engine.setScreenDistance(d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_EYE_SPACING] = new Parameter(this, 10) {
            public double get() {
                return options.os().eyeSpacing;
            }

            public void set(double d) {
                options.os().eyeSpacing = d;
            }

            public void apply(double d) {
                engine.setEyeSpacing(d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_TILT_VERTICAL] = new Parameter(this, 10) {
            public double get() {
                return options.os().tiltVertical;
            }

            public void set(double d) {
                options.os().tiltVertical = d;
            }

            public void apply(double d) {
                engine.setTiltVertical(d);
            }
        };
        parameter[OptionsKeysConfig.PARAM_TILT_HORIZONTAL] = new Parameter(this, 10) {
            public double get() {
                return options.os().tiltHorizontal;
            }

            public void set(double d) {
                options.os().tiltHorizontal = d;
            }

            public void apply(double d) {
                engine.setTiltHorizontal(d);
            }
        };

        textureParameter = new Parameter[10];
        for (int i = 0; i < 10; i++) {
            textureParameter[i] = new TextureParameter(this, i);
        }
    }

    public void changeParameter(int param, int sign) {
        // caller must prevent PARAM_NONE
        changeParameter(parameter[param], sign);
    }

    public void changeTexture(int i) {
        changeParameter(textureParameter[i], 0); // sign is ignored
    }

    private void changeParameter(Parameter p, int sign) {

        double dSave = p.get();
        double dNew = p.change(dSave, sign);

        p.set(dNew);

        boolean isValid = false;
        try {
            p.getOptions().validate();
            isValid = true;
        } catch (ValidationException ignored) {
        }

        if (isValid) p.apply(dNew);
        else p.set(dSave);
    }

    // menu commands must run later, otherwise there would be confusion.
    // in practice only newGame is a problem, but I think the principle is valid.
    //
    // there are two problems with calling newGame earlier.
    //
    // on the one hand, it would lead to setOptions and reset getting called,
    // modifying the object while we're in the middle of execution.
    // commandActive would get set to null, and alignMode might change.
    //
    // on the other hand, after it returned, we would continue executing,
    // so we might apply some random commands to the new game.
    // we might, for example, run a single step of non-aligned movement.

    // --- parameter changes ---

}

