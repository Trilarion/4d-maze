/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

public class CommandRotate extends Command {
    private final Controller controller;
    private final int a1;
    private final int a2;
    private final int sign;

    public CommandRotate(Controller controller, int a1, int a2, int sign) {
        this.controller = controller;
        this.a1 = a1;
        this.a2 = a2;
        this.sign = sign;
    }

    public boolean isExclusive() {
        return controller.alignMode;
    }

    public void activate() {
        // only called in align mode
        controller.nActive = controller.nRotate;
        controller.dActive = controller.dRotate;
    }

    public boolean run() {
        int a1 = (this.a1 == -1) ? controller.dim - 1 : this.a1;
        int a2 = (this.a2 == -1) ? controller.dim - 1 : this.a2;
        if (controller.alignMode) {
            controller.engine.rotateAngle(a1, a2, sign * controller.dActive);
            --controller.nActive;
            if (controller.nActive > 0) {
                return true;
            } else {
                controller.engine.align().snap();
                return false;
            }
        } else {
            controller.engine.rotateAngle(a1, a2, sign * controller.dRotate);
            return false;
        }
    }
}
