/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

import org.maze4d.options.OptionsKeysConfig;

public class CommandChangeParameter extends Command {
    private final Controller controller;
    private final int i;
    private final int sign;

    public CommandChangeParameter(Controller controller, int i, int sign) {
        this.controller = controller;
        this.i = i;
        this.sign = sign;
    }

    public boolean isOnPress() {
        return (controller.param[i] == OptionsKeysConfig.PARAM_COLOR_MODE
                || controller.param[i] == OptionsKeysConfig.PARAM_DEPTH);
    }

    public boolean canRun() {
        return (controller.param[i] != OptionsKeysConfig.PARAM_NONE);
    }

    public boolean isExclusive() {
        return false;
    }

    public boolean isExcluded() {
        return false;
    }

    public boolean run() {
        controller.changeParameter(controller.param[i], sign);
        return false;
    }
}
