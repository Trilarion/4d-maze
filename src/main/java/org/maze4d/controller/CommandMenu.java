/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

import java.awt.*;

public class CommandMenu extends Command {
    private final Runnable r;

    public CommandMenu(Runnable r) {
        this.r = r;
    }

    public boolean isOnPress() {
        return true;
    }

    public boolean isImmediate() {
        return true;
    }

    public boolean isExclusive() {
        return false;
        // you might think you'd want exclusive access,
        // but the real menus don't have that either.
        // actually, this is academic because of isImmediate
    }

    public boolean run() {
        EventQueue.invokeLater(r); // see note below
        return false;
    }
}
