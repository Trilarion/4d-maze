/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

public class CommandMove extends Command {
    private final Controller controller;
    private final int a;
    private final int sign;

    public CommandMove(Controller controller, int a, int sign) {
        this.controller = controller;
        this.a = a;
        this.sign = sign;
    }

    public boolean canRun() {
        int a = (this.a == -1) ? controller.dim - 1 : this.a;
        if (controller.alignMode) {
            return controller.engine.canMove(a, sign);
        } else {
            return true;
        }
    }

    public boolean isExclusive() {
        return controller.alignMode;
    }

    public void activate() {
        // only called in align mode
        controller.nActive = controller.nMove;
        controller.dActive = controller.dMove;
    }

    public boolean run() {
        int a = (this.a == -1) ? controller.dim - 1 : this.a;
        if (controller.alignMode) {
            controller.engine.move(a, sign * controller.dActive);
            --controller.nActive;
            if (controller.nActive > 0) {
                return true;
            } else {
                controller.engine.align().snap();
                return false;
            }
        } else {
            controller.engine.move(a, sign * controller.dMove);
            return false;
        }
    }
}
