/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

/**
 *
 */
abstract class Command {
    public boolean isOnPress() {
        return false;
    }

    public boolean canRun() {
        return true;
    }

    public boolean isImmediate() {
        return false;
    }

    public abstract boolean isExclusive();

    public boolean isExcluded() {
        return true;
    }

    public void activate() {
    }

    public abstract boolean run(); // like org.maze4d.IClock, true if want more time
}
