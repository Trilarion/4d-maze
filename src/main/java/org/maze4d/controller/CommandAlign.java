/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

public class CommandAlign extends Command {
    private final Controller controller;

    public CommandAlign(Controller controller) {
        this.controller = controller;
    }

    public boolean isOnPress() {
        return true;
    }

    public boolean canRun() {
        return !controller.alignMode;
    }

    public boolean isExclusive() {
        return true;
        // when it can run, it is always exclusive
        // (the functions are called in order)
    }

    public void activate() {
        // we don't need to cache the distances,
        // the alignment will turn out correctly even if they change
        controller.alignActive = controller.engine.align();
    }

    public boolean run() {
        if (!controller.alignActive.align(controller.dAlignMove, controller.dAlignRotate)) {
            return true;
        } else {
            controller.alignActive = null; // done, release reference
            return false;
        }
    }
}
