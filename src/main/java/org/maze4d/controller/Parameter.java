/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

import org.maze4d.ui.Validate;

public abstract class Parameter {

    private final Controller controller;
    private final int grain;

    protected Parameter(Controller controller, int grain) {
        this.controller = controller;
        this.grain = grain;
    }

    public Validate getOptions() {
        return controller.options.os();
    } // correct for five of nine

    public abstract double get();

    public abstract void set(double d);

    public double change(double d, int sign) {
        return (Math.rint(d * grain) + sign) / grain;
        // the reason for this slightly peculiar method
        // is that it produces nice round FP numbers
    }

    public abstract void apply(double d);
}
