/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.controller;

public class CommandChangeAlignMode extends Command {
    private final Controller controller;

    public CommandChangeAlignMode(Controller controller) {
        this.controller = controller;
    }

    public boolean isOnPress() {
        return true;
    }

    public boolean isExclusive() {
        return !controller.alignMode;
    }

    public void activate() {
        // only called in non-align mode
        controller.alignActive = controller.engine.align();
    }

    public boolean run() {
        if (controller.alignMode) {
            controller.alignMode = false;
            return false;
        } else {
            if (!controller.alignActive.align(controller.dAlignMove, controller.dAlignRotate)) {
                return true;
            } else {
                controller.alignActive = null; // done, release reference

                // it's important that we don't set align mode until the end.
                // among other things, this stops the user from obtaining a bad state
                // by saving the game while an alignment is in progress.

                controller.alignMode = true;
                return false;
            }
        }
    }
}
