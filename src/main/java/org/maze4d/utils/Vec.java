/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.util.Arrays;

/**
 * A utility class containing various operations on points and lines.
 * Except as noted, it is safe for the source and destination arrays to be the same.
 */
public final class Vec {
    private Vec() {
    }

// --- non-mathematical operations ---

    public static void zero(double[] dest) {
        Arrays.fill(dest, 0);
    }

    public static void copy(double[] dest, double[] src) {
        // could use System.arraycopy
        System.arraycopy(src, 0, dest, 0, dest.length);
    }

// --- unit vectors ---

    public static void unitVector(double[] dest, int a) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = (i == a) ? 1 : 0;
        }
    }

    public static void unitMatrix(double[][] dest) {
        for (int i = 0; i < dest.length; i++) {
            unitVector(dest[i], i);
        }
    }

// --- simple arithmetic ---

    public static void add(double[] dest, double[] src1, double[] src2) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = src1[i] + src2[i];
        }
    }

    public static void sub(double[] dest, double[] src1, double[] src2) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = src1[i] - src2[i];
        }
    }

    public static void scale(double[] dest, double[] src, double scale) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = scale * src[i];
        }
    }

    public static void addScaled(double[] dest, double[] src1, double[] src2, double scale) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = src1[i] + scale * src2[i];
        }
    }

// --- vector arithmetic ---

    public static double dot(double[] p1, double[] p2) {
        double sum = 0;
        for (int i = 0; i < p1.length; i++) {
            sum += p1[i] * p2[i];
        }
        return sum;
    }

    public static double norm(double[] p) {
        return Math.sqrt(dot(p, p));
    }

    public static void normalize(double[] dest, double[] src) {
        scale(dest, src, 1 / norm(src));
    }

// --- rotation ---

    /**
     * Rotate src1 toward src2 using the given cosine and sine.
     * The vectors src1 and src2 should be orthogonal and have the same length.
     */
    public static void rotateCosSin(double[] dest1, double[] dest2, double[] src1, double[] src2, double cos, double sin) {
        for (int i = 0; i < dest1.length; i++) {
            double s1 = src1[i];
            double s2 = src2[i];
            dest1[i] = cos * s1 + sin * s2;
            dest2[i] = cos * s2 - sin * s1;
        }
    }

    /**
     * Rotate src1 toward src2 by theta degrees.
     * The vectors src1 and src2 should be orthogonal and have the same length.
     */
    public static void rotateAngle(double[] dest1, double[] dest2, double[] src1, double[] src2, double theta) {
        theta = Math.toRadians(theta);
        rotateCosSin(dest1, dest2, src1, src2, Math.cos(theta), Math.sin(theta));
    }

    /**
     * Rotate src1 toward src2 so that src1 points at the point with coordinates (x1,x2).
     * The vectors src1 and src2 should be orthogonal and have the same length.
     */
    public static void rotatePoint(double[] dest1, double[] dest2, double[] src1, double[] src2, double x1, double x2) {
        double r = Math.sqrt(x1 * x1 + x2 * x2);
        rotateCosSin(dest1, dest2, src1, src2, x1 / r, x2 / r);
    }

// --- projection ---

    /**
     * Project a vector onto a screen at a given distance,
     * using lines radiating from the origin.
     * The result vector has lower dimension than the original.
     */
    public static void projectDistance(double[] dest, double[] src, double distance) {
        double scale = distance / src[dest.length];

        // same as scale, but with different-sized arrays
        for (int i = 0; i < dest.length; i++) {
            dest[i] = scale * src[i];
        }
    }

    /**
     * Project a vector onto a retina at distance 1,
     * using lines radiating from the origin,
     * and then scale so the retina has size 1.
     * The result vector has lower dimension than the original.
     */
    public static void projectRetina(double[] dest, double[] src, double retina) {
        projectDistance(dest, src, 1 / retina);
    }

// --- coordinate conversion ---

    /**
     * Express a vector in terms of a set of axes.
     * The vectors src and dest must be different objects.
     */
    public static void toAxisCoordinates(double[] dest, double[] src, double[][] axis) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = dot(axis[i], src);
        }
    }

    /**
     * Take a vector expressed in terms of a set of axes
     * and convert it back to the original coordinate system.
     * The vectors src and dest must be different objects.
     */
    public static void fromAxisCoordinates(double[] dest, double[] src, double[][] axis) {
        zero(dest);
        for (int i = 0; i < dest.length; i++) {
            addScaled(dest, dest, axis[i], src[i]);
        }
    }

// --- clipping ---

    /**
     * Find the point that is a fraction f of the way from src1 to src2.
     */
    public static void mid(double[] dest, double[] src1, double[] src2, double f) {
        for (int i = 0; i < dest.length; i++) {
            dest[i] = src1[i] + f * (src2[i] - src1[i]);
        }
    }

    /**
     * Clip the line from p1 to p2 into the half-space that the vector n points into.
     * The vector n does not need to be normalized.<p>
     * Since clipping often does nothing, the vectors p1 and p2 are modified in place.
     *
     * @return True if the line is entirely removed by clipping.
     */
    public static boolean clip(double[] p1, double[] p2, double[] n) {

        double d1 = dot(p1, n);
        double d2 = dot(p2, n);

        if (d1 >= 0) {
            if (d2 >= 0) {
                // not clipped
            } else {
                mid(p2, p2, p1, d2 / (d2 - d1));
            }
        } else {
            if (d2 >= 0) {
                mid(p1, p1, p2, d1 / (d1 - d2));
            } else {
                // completely clipped
                return true;
            }
        }

        return false;
    }

}

