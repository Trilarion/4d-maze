/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import org.maze4d.ValidationException;
import org.maze4d.options.OptionsImage;

import java.io.*;

/**
 * A utility class for loading and saving PostScript files.
 */
public final class PostScriptFile {

    private PostScriptFile() {
    }

    public static void save(File file, int n, LineBuffer[] buf,
                            int edge, int gap, double screenWidth, OptionsImage oi) throws ValidationException {
        try {

            try (OutputStream stream = new FileOutputStream(file)) {

                PostScriptPrinter printer = new PostScriptPrinter(new PrintStream(stream), edge, gap, screenWidth, oi);

                printer.printHeader(n);
                for (int i = 0; i < n; i++) {
                    printer.print(buf[i]);
                }

            }

        } catch (IOException e) {
            throw App.getException("PostScriptFile.e1", new Object[]{file.getName(), e.getMessage()});
        }
    }

}

