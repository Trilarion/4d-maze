/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.awt.*;

/**
 * An object that represents a colored line in some number of dimensions.
 */
public class Line {

    private final double[] p1; // TODO p1, p2 could be Positions
    private final double[] p2;
    private Color color;

    public Line(double[] p1, double[] p2, Color color) {
        this.p1 = p1;
        this.p2 = p2;
        this.color = color;
    }

    public double[] getP1() {
        return p1;
    }

    public double[] getP2() {
        return p2;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

