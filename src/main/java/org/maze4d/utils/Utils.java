/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.io.File;

/**
 * Utility class for everything that doesn't fit somewhere else.
 */
public final class Utils {

    public static final String suffix = ".eps";

    private Utils() {
    }

    public static File toFile(String s) {
        return (s.isEmpty()) ? null : new File(s);
    }

    public static String toString(File file) {
        return (file == null) ? "" : file.getPath();
    }

    public static boolean hasSuffix(File f) {
        return f.isDirectory() || f.getName().endsWith(suffix);
    }

    public static File forceSuffix(File f) {
        return hasSuffix(f) ? f : new File(f.getParentFile(), f.getName() + suffix);
    }

    /**
     * Join parts of two arrays together (by concatenation).
     */
    public static int[] join(int[] a1, int base1, int n1, int[] a2, int base2, int n2) {
        int[] a = new int[n1 + n2];

        int next = 0;
        for (int i = 0; i < n1; i++) {
            a[next] = a1[base1 + i];
            next++;
        }
        for (int i = 0; i < n2; i++) {
            a[next] = a2[base2 + i];
            next++;
        }

        return a;
    }
}
