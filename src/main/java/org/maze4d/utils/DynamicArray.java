/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.util.Random;

/**
 * A utility class for manipulating arrays with variable numbers of dimensions.
 */
public final class DynamicArray {

    private DynamicArray() {
    }

    public static int[] makeLimits(int[] size) {

        // we don't need dimSpace and dimMap as arguments,
        // we know by construction that dimSpace = size.length
        // and by validation that size[i] = 1 for i >= dimMap

        int[] limits = new int[size.length];
        for (int i = 0; i < size.length; i++) limits[i] = size[i] + 2;
        return limits;
    }

    /**
     * Check whether a cell is in the interior of an array.
     */
    public static boolean inBounds(int[] p, int[] limits) {
        for (int i = 0; i < limits.length; i++) {
            if (p[i] < 1 || p[i] > limits[i] - 2) return false;
        }
        return true;
    }

    /**
     * Pick a random cell in the interior of an array.
     */
    public static int[] pick(int[] limits, Random random) {
        int[] p = new int[limits.length];
        for (int i = 0; i < limits.length; i++) p[i] = 1 + random.nextInt(limits[i] - 2);
        return p;
    }

}

