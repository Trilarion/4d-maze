/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.awt.*;
import java.util.ArrayList;

/**
 * A line buffer that reuses line objects to avoid memory allocation.
 */
public class LineBuffer { // TODO what about ArrayList instead?

    private final int dim;
    private final java.util.List<Line> lines;
    private int size;

    public LineBuffer(int dim) {
        this.dim = dim;
        lines = new ArrayList<>();
        size = 0;
    }

// --- methods for writing to the buffer ---

    public void clear() {
        size = 0;
        // do not clear the vector, we reuse the elements
    }

    /**
     * Get a line object to write into, allocating a new one if necessary.
     */
    public Line getNext() {
        Line line;

        if (size < lines.size()) {
            line = lines.get(size);

        } else { // size == lines.size()
            line = new Line(new double[dim], new double[dim], null);
            lines.add(line);
        }

        size++;
        return line;
    }

    /**
     * Back up after a call to getNext turns out to be unnecessary.
     */
    public void unget() {
        size--;
    }

    /**
     * Add a line to the buffer.
     * Ownership of the arguments p1 and p2 is not transferred,
     * so it is OK to keep and modify them after calling add.
     */
    public void add(double[] p1, double[] p2, Color color) {
        Line line = getNext();

        System.arraycopy(p1, 0, line.getP1(), 0, dim);
        System.arraycopy(p2, 0, line.getP2(), 0, dim);
        line.setColor(color);
    }

// --- methods for reading from the buffer ---

    public int size() {
        return size;
    }

    public Line get(int i) {
        return lines.get(i);
    }

}

