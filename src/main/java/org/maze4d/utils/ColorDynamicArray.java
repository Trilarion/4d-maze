/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

import java.awt.*;

public class ColorDynamicArray {

    private final int dim;
    private final int[] limits;
    private final Object data;

    public ColorDynamicArray(int dim, int[] limits) {
        this.dim = dim;
        this.limits = limits;
        if (dim == 3) {
            data = new Color[limits[0]][limits[1]][limits[2]];
        } else {
            data = new Color[limits[0]][limits[1]][limits[2]][limits[3]];
        }
    }

    // --- accessors ---

    public Color get(int[] p) {
        if (dim == 3) {
            return ((Color[][][]) data)[p[0]][p[1]][p[2]];
        } else {
            return ((Color[][][][]) data)[p[0]][p[1]][p[2]][p[3]];
        }
    }

    public void set(int[] p, Color color) {
        if (dim == 3) {
            ((Color[][][]) data)[p[0]][p[1]][p[2]] = color;
        } else {
            ((Color[][][][]) data)[p[0]][p[1]][p[2]][p[3]] = color;
        }
    }

    public boolean inBounds(int[] p) {
        return DynamicArray.inBounds(p, limits);
    }
}
