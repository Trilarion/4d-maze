/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

public class BooleanDynamicArray {

    private final int dim;
    private final int[] limits;
    private final Object data;

    public BooleanDynamicArray(int dim, int[] limits) {
        this.dim = dim;
        this.limits = limits;
        if (dim == 3) {
            data = new boolean[limits[0]][limits[1]][limits[2]];
        } else {
            data = new boolean[limits[0]][limits[1]][limits[2]][limits[3]];
        }
    }

    // --- accessors ---

    public boolean get(int[] p) {
        if (dim == 3) {
            return ((boolean[][][]) data)[p[0]][p[1]][p[2]];
        } else {
            return ((boolean[][][][]) data)[p[0]][p[1]][p[2]][p[3]];
        }
    }

    public void set(int[] p, boolean b) {
        if (dim == 3) {
            ((boolean[][][]) data)[p[0]][p[1]][p[2]] = b;
        } else {
            ((boolean[][][][]) data)[p[0]][p[1]][p[2]][p[3]] = b;
        }
    }

    public boolean inBounds(int[] p) {
        return DynamicArray.inBounds(p, limits);
    }
}
