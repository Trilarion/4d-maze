/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.utils;

/**
 * An object that iterates over an arbitrary-dimensional subspace of an array.
 */
public class DynamicArrayIterator {

    private final int[] a;
    private final int[] i;
    private final int[] limits;
    private boolean done;

    /**
     * @param a      The axes that define the subspace to iterate over.
     * @param i      The initial point.
     *               i[a] should be zero for all elements of the array a.
     * @param limits The limits that define the size of the array.
     */
    public DynamicArrayIterator(int[] a, int[] i, int[] limits) {
        this.a = a;
        this.i = i.clone();
        this.limits = limits;
        done = false;
    }

    public boolean hasCurrent() {
        return (!done);
    }

    public int[] current() {
        return i; // caller shouldn't modify
    }

    public void increment() {

        // this is just adding one to a number with digits in different bases

        for (int k : a) {
            ++i[k];
            if (i[k] < limits[k]) return; // no carry
            i[k] = 0;
        }
        // overflow

        // when iterating over no dimensions, there is exactly one iteration;
        // iterating over any number of dimensions when the limits are 1 is the same

        done = true;
    }
}
