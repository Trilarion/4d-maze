/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.ValidationException;

/**
 * An interface for objects that want to run custom validations when loaded from a org.maze4d.property.PropertyStore.
 */
public interface Validate {

    void validate() throws ValidationException;

}

