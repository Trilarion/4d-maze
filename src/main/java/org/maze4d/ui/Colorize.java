/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import java.awt.*;

/**
 * An interface for determining face colors.
 */
public interface Colorize {

    Color getColor(int[] p, int dir);

}
