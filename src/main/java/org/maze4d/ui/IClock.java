/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

/**
 * An interface for things that can be driven by a clock.
 */
public interface IClock {

    /**
     * Perform one tick's worth of work.
     *
     * @return True if more work is pending, false if idle.
     */
    boolean tick();

}

