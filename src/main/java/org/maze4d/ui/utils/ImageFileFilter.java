/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.utils;

import org.maze4d.utils.App;
import org.maze4d.utils.Utils;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ImageFileFilter extends FileFilter {
    public boolean accept(File f) {
        return Utils.hasSuffix(f);
    }

    public String getDescription() {
        return App.getString("Maze.s20");
    }
}
