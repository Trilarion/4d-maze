/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.utils;

import org.maze4d.ui.IClock;

import java.awt.*;

/**
 * A clock, i.e., an object that ticks at a given rate (except when it's idle).<p>
 *
 * javax.swing.Timer is supposed to do approximately the same thing, but ...
 * I didn't feel like I had complete control over the last tick, the coalesce behavior wasn't clear,
 * and, worst of all, when I tried it, it consistently ran slow.
 * Maybe it wasn't taking into account the time used by the action being performed?
 */
public class Clock {

    private final IClock clockInterface;
    private int interval; // ms

    // synchronized access
    private boolean eventStart; // main -> clock
    private boolean eventTick;
    private boolean shouldIdle;
    private boolean isIdle;     // clock -> main

    public Clock(IClock clockInterface) {
        this.clockInterface = clockInterface;
        // interval initialized via setFrameRate

        Thread thread = new Thread(this::thread);

        eventStart = false;
        eventTick = false;
        // shouldIdle is only valid when eventTick is true
        isIdle = false; // let thread set correct value

        thread.start();
        try {
            waitIdle();
        } catch (InterruptedException e) {
            // won't happen
        }
    }

// --- main thread ---

    public void setFrameRate(double frameRate) {
        interval = (int) Math.ceil(1000 / frameRate);
    }

    /**
     * Start the clock, and continue running until the clock interface reports idleness.
     */
    public void start() {
        signalStart();
    }

    private void tick() {
        signalResult( /* shouldIdle = */ !clockInterface.tick());
    }

// --- synchronization ---

    private synchronized void signalStart() {
        if (!isIdle) return; // already started

        eventStart = true;
        notify();
    }

    private synchronized void signalResult(boolean shouldIdle) {
        // no need to check thread state, we know it because tick was called

        this.shouldIdle = shouldIdle;

        eventTick = true;
        notify();
    }

    private synchronized void waitIdle() throws InterruptedException {
        while (!isIdle) wait();
    }

    private synchronized void waitStart() throws InterruptedException {
        isIdle = true;
        notify(); // to release constructor wait

        while (!eventStart) wait();
        eventStart = false;

        isIdle = false;
    }

    private synchronized boolean waitResult() throws InterruptedException {

        while (!eventTick) wait();
        eventTick = false;

        return shouldIdle;
    }

// --- clock thread ---

    private void thread() {
        Runnable runTick = this::tick;
        try {
            while (true) {

                waitStart();

                long base = System.currentTimeMillis();
                while (true) {

                    EventQueue.invokeLater(runTick);
                    if ( /* shouldIdle = */ waitResult()) break;

                    long now = System.currentTimeMillis();
                    long next = base + interval; // unsynchronized use of interval is OK

                    if (now < next) { // we have time, sleep a bit
                        Thread.sleep(next - now);
                        base = next; // same equation as below would work, but this is clearer
                    } else { // no time left, tick again immediately
                        base = Math.max(next, now - 3 * interval); // see note below
                    }

                    // on my system, the actual sleep duration is granular,
                    // with each grain being approximately 55 ms.
                    // so, if you ask to sleep for 100 ms, you usually sleep for 110.

                    // the code above is designed to compensate for this.
                    // as long as we are producing frames sufficiently quickly,
                    // base will advance by the exact interval,
                    // so oversleeping will lead to requesting shorter wait times.

                    // if we are not producing frames quickly enough,
                    // there's no sense accumulating a large sleep debt,
                    // just go ahead and reset the base.
                    // actually, it would be nice to be able to recover from a few slow frames,
                    // so do let debt accumulate, but limit it to a fixed number of multiples
                }

            }
        } catch (InterruptedException e) {
            // won't happen
        }
    }

}

