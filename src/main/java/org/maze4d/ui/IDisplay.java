/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.utils.LineBuffer;

/**
 * An interface for controlling a set of displays on screen
 * without really knowing anything about them.
 */
public interface IDisplay {

    void setMode3D(LineBuffer buf);

    void setMode4DMono(LineBuffer buf);

    void setMode4DStereo(LineBuffer buf1, LineBuffer buf2);

    void nextFrame();

}

