/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.dialog;

import org.maze4d.ui.utils.GridBagHelper;
import org.maze4d.utils.App;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * A dialog for showing information about the game.
 */
public class DialogAbout extends JDialog {

    public DialogAbout(Frame owner) {
        super(owner, s("s1"), true);
        setResizable(false);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                doOK();
            }
        });

        // create main panel

        JPanel panelMain = new JPanel();
        panelMain.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagHelper helper = new GridBagHelper(panelMain);
        int y = 0;

        helper.addCenter(0, y, ls("s2"));
        y++;
        helper.addCenter(0, y, l(App.getString("URL")));
        y++;
        helper.add(0, y, Box.createVerticalStrut(10));
        y++;
        helper.addCenter(0, y, ls("s3"));
        y++;
        helper.addCenter(0, y, ls("s4"));
        y++;
        helper.add(0, y, Box.createVerticalStrut(10));
        y++;
        helper.addCenter(0, y, ls("s5"));
        y++;
        helper.addCenter(0, y, ls("s8"));
        y++;
        helper.add(0, y, Box.createVerticalStrut(10));
        y++;
        helper.addCenter(0, y, ls("s6"));

        // create buttons

        JPanel panelButton = new JPanel();
        JButton button;

        button = new JButton(s("s7"));
        button.addActionListener(e -> doOK());
        panelButton.add(button);

        // add to content pane

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        contentPane.add(panelMain, BorderLayout.CENTER);
        contentPane.add(panelButton, BorderLayout.PAGE_END);

        // finish up

        pack();
        setLocationRelativeTo(owner);
    }

    private static String s(String key) {
        return App.getString("DialogAbout." + key);
    }

    private static JLabel l(String text) {
        return new JLabel(text);
    }

    private static JLabel ls(String key) {
        return l(s(key));
    }

    private void doOK() {
        dispose();
    }

}

