/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.ValidationException;
import org.maze4d.model.Align;
import org.maze4d.model.Map;
import org.maze4d.options.*;
import org.maze4d.property.Store;
import org.maze4d.ui.display.Display;
import org.maze4d.ui.display.DisplayScaled;
import org.maze4d.ui.display.DisplayStereo;
import org.maze4d.utils.*;

import java.awt.*;

/**
 * The game graphics engine.
 */
public class Engine {

    private static final String KEY_ORIGIN = "origin";
    private static final String KEY_AXIS = "axis";
    private static final String KEY_WIN = "win";
    private static final int DISPLAY_MODE_NONE = 0;
    private static final int DISPLAY_MODE_3D = 1;
    private static final int DISPLAY_MODE_4D_MONO = 2;
    private static final int DISPLAY_MODE_4D_STEREO = 3;
    private static final double[][] objRetina2 = {
            {-1, -1}, {1, -1},
            {1, -1}, {1, 1},
            {1, 1}, {-1, 1},
            {-1, 1}, {-1, -1}
    };
    private static final double[][] objRetina3 = {
            {-1, -1, -1}, {1, -1, -1},
            {1, -1, -1}, {1, 1, -1},
            {1, 1, -1}, {-1, 1, -1},
            {-1, 1, -1}, {-1, -1, -1},

            {-1, -1, 1}, {1, -1, 1},
            {1, -1, 1}, {1, 1, 1},
            {1, 1, 1}, {-1, 1, 1},
            {-1, 1, 1}, {-1, -1, 1},

            {-1, -1, -1}, {-1, -1, 1},
            {1, -1, -1}, {1, -1, 1},
            {1, 1, -1}, {1, 1, 1},
            {-1, 1, -1}, {-1, 1, 1}
    };
    private static final double B = 0.04;
    private static final double[][] objCross2 = {
            {-B, 0}, {B, 0},
            {0, -B}, {0, B}
    };
    private static final double C = 0.1;
    private static final double[][] objCross3 = {
            {-C, 0, 0}, {C, 0, 0},
            {0, -C, 0}, {0, C, 0},
            {0, 0, -C}, {0, 0, C}
    };
    private static final double[][] objWin2 = {
            {-0.8, 0.4}, {-0.8, -0.4},
            {-0.8, -0.4}, {-0.6, 0},
            {-0.6, 0}, {-0.4, -0.4},
            {-0.4, -0.4}, {-0.4, 0.4},

            {-0.1, 0.4}, {0.1, 0.4},
            {0, 0.4}, {0, -0.4},
            {-0.1, -0.4}, {0.1, -0.4},

            {0.4, -0.4}, {0.4, 0.4},
            {0.4, 0.4}, {0.8, -0.4},
            {0.8, -0.4}, {0.8, 0.4}
    };
    private static final double[][] objWin3 = {
            {-0.8, 0.4, 1}, {-0.8, -0.4, 1},
            {-0.8, -0.4, 1}, {-0.6, 0, 1},
            {-0.6, 0, 1}, {-0.4, -0.4, 1},
            {-0.4, -0.4, 1}, {-0.4, 0.4, 1},

            {-0.1, 0.4, 1}, {0.1, 0.4, 1},
            {0, 0.4, 1}, {0, -0.4, 1},
            {-0.1, -0.4, 1}, {0.1, -0.4, 1},

            {0.4, -0.4, 1}, {0.4, 0.4, 1},
            {0.4, 0.4, 1}, {0.8, -0.4, 1},
            {0.8, -0.4, 1}, {0.8, 0.4, 1}
    };
    private final IDisplay displayInterface;
    private Map map;
    private Colorizer colorizer;
    private double[] origin;
    private double[][] axis;

    private boolean win;

    // --- games ---
    private LineBuffer bufAbsolute;
    private LineBuffer bufRelative;
    private LineBuffer[] bufDisplay;
    private RenderAbsolute renderAbsolute;

        private RenderRelative renderRelative;
    private double[][] objRetina, objCross, objWin;
    private Display[] display;
    private int dimSpaceCache; // display-related cache of engine-level fields
    private boolean enableCache;

    private int edgeCache;
    private int[] reg1; // temporary registers
    private int[] reg2;
    private double[] reg3;
    private double[] reg4;

    /**
     * Construct a new engine object.
     * After construction, you must call newGame before anything else.
     */
    public Engine(IDisplay displayInterface) {
        this.displayInterface = displayInterface;

        // dimSpaceCache starts at zero, that will force rebuild ...
        //  ... so enableCache is irrelevant
        // edgeCache starts at zero
    }

    public void newGame(int dimSpace, OptionsMap om,
                        OptionsColor oc, OptionsView ov, OptionsStereo os, OptionsSeed oe,
                        boolean render) {

        map = new Map(dimSpace, om, oe.mapSeed);
        colorizer = new Colorizer(dimSpace, om.dimMap, om.size, oc, oe.colorSeed);

        origin = new double[dimSpace];
        axis = new double[dimSpace][dimSpace];
        initPlayer();

        bufAbsolute = new LineBuffer(dimSpace);
        bufRelative = new LineBuffer(dimSpace - 1);
        bufDisplay = new LineBuffer[2];
        bufDisplay[0] = new LineBuffer(2);
        bufDisplay[1] = new LineBuffer(2); // may not be used

        renderAbsolute = new RenderAbsolute(bufAbsolute, dimSpace, map, colorizer, ov);
        renderRelative = new RenderRelative(bufAbsolute, bufRelative, dimSpace, ov.retina);

        if (dimSpace == 3) {
            objRetina = objRetina2;
            objCross = objCross2;
            objWin = objWin2;
        } else {
            objRetina = objRetina3;
            objCross = objCross3;
            objWin = objWin3;
        }

        setDisplay(dimSpace, ov.scale, os, true);

        reg1 = new int[dimSpace];
        reg2 = new int[dimSpace];
        reg3 = new double[dimSpace];
        reg4 = new double[dimSpace];

        if (render) renderAbsolute();
        // else we are loading a saved game, and will render later
    }

    private void initPlayer() {

        Grid.fromCell(origin, map.getStart());

        // cycle the axes so that we're correctly oriented when dimMap < dimSpace
        // axis[dimSpace-1] points in the forward direction, which should be unitVector(0) ... etc.
        // everything else is random, so it's OK for the axes to be deterministic
        //
        for (int a = 0; a < axis.length; a++) Vec.unitVector(axis[a], (a + 1) % axis.length);

        win = false;
    }

    public void resetWin() {
        if (win && !atFinish()) {
            win = false;
            renderRelative();
        }
    }

    public void restartGame() {
        initPlayer();
        renderAbsolute();
    }

    // these last two option-setting functions are not invoked via the controller,
    // so they need to end with an explicit re-render call

    public void load(Store store, boolean alignMode) throws ValidationException {
        try {

            store.getObject(KEY_ORIGIN, origin);
            store.getObject(KEY_AXIS, axis);
            win = store.getBoolean(KEY_WIN);

            // check that origin is in bounds and open
            // this is clumsy, because normally the walls keep us in bounds.

            // we might be on multiple boundaries, in which case toCell can't return all cells,
            // but that doesn't matter here.
            // the cells are all adjacent, so if any one is strictly in bounds,
            // the rest are enough in bounds not to cause an array fault in isOpen.

            Grid.toCell(reg1, reg2, origin); // ignore result
            if (!map.inBounds(reg1)) throw App.getEmptyException();

            if (!Grid.isOpen(origin, map, reg1)) throw App.getEmptyException();

            // check that axes are orthonormal, more or less
            final double EPSILON = 0.001;
            for (int i = 0; i < axis.length; i++) {
                for (int j = 0; j < axis.length; j++) {
                    double dotExpected = (i == j) ? 1 : 0; // delta_ij
                    double dot = Vec.dot(axis[i], axis[j]);
                    if (Math.abs(dot - dotExpected) > EPSILON) throw App.getEmptyException();
                }
            }

            if (alignMode) align().snap();
            //
            // pseudo-validation to prevent being in align mode without being aligned.
            // this can only happen if someone modifies a file by hand
            //
            // a real validation would compare the current position to the align goal,
            // and if they were different, would snap to the goal and throw an exception
            // carrying a message similar to org.maze4d.ui.Engine.e1

        } catch (ValidationException e) {
            initPlayer();
            throw App.getException("Engine.e1"); // slight misuse of protocol to report information
        } finally {
            renderAbsolute();
        }
    }

    public void save(Store store) throws ValidationException {

        store.putObject(KEY_ORIGIN, origin);
        store.putObject(KEY_AXIS, axis);
        store.putBoolean(KEY_WIN, win);
    }

// --- display ---

    public void setColorMode(int colorMode) {
        colorizer.setColorMode(colorMode);
    }

    public void setDepth(int depth) {
        renderAbsolute.setDepth(depth);
    }

    public void setTexture(boolean[] texture) {
        renderAbsolute.setTexture(texture);
    }

    public void setRetina(double retina) {
        renderRelative.setRetina(retina);
    }

    public void setScale(double scale) {
        for (Display value : display) value.setScale(scale);
    }

    public void setScreenWidth(double screenWidth) {
        for (Display value : display) value.setScreenWidth(screenWidth);
    }

    public void setScreenDistance(double screenDistance) {
        for (Display value : display) value.setScreenDistance(screenDistance);
    }

    public void setEyeSpacing(double eyeSpacing) {
        for (Display value : display) value.setEyeSpacing(eyeSpacing);
    }

// --- motion ---

    public void setTiltVertical(double tiltVertical) {
        for (Display value : display) value.setTiltVertical(tiltVertical);
    }

    public void setTiltHorizontal(double tiltHorizontal) {
        for (Display value : display) value.setTiltHorizontal(tiltHorizontal);
    }

    public void setOptions(OptionsColor oc, OptionsView ov, OptionsStereo os, OptionsSeed oe) {

        colorizer.setOptions(oc, oe.colorSeed);

        renderAbsolute.setDepth(ov.depth);
        renderAbsolute.setTexture(ov.texture);

        renderRelative.setRetina(ov.retina);

        setDisplay(dimSpaceCache, ov.scale, os, false);

        renderAbsolute(); // not always necessary, but who cares, it's fast enough
    }

    public void setEdge(int edge) {
        edgeCache = edge;
        for (Display value : display) value.setEdge(edge);
        renderDisplay();
    }

    private int getDisplayMode(int dimSpace, boolean enable) {
        switch (dimSpace) {
            case 3:
                return DISPLAY_MODE_3D;
            case 4:
                return enable ? DISPLAY_MODE_4D_STEREO : DISPLAY_MODE_4D_MONO;
            default:
                return DISPLAY_MODE_NONE;
        }
    }

// --- rendering ---

    private int getPanels(int mode) {
        return (mode == DISPLAY_MODE_4D_STEREO) ? 2 : 1;
    }

    private void setDisplay(int dimSpace, double scale, OptionsStereo os, boolean force) {

        int modeNew = getDisplayMode(dimSpace, os.enable);
        int modeOld = getDisplayMode(dimSpaceCache, enableCache);

        // here we are embedding the knowledge that the edge changes (and setEdge gets called)
        // if and only if the number of visible panels changes.
        // so, that's the condition we should use to decide when to clear the cache.
        // the goal is not to draw stereo displays until after re-layout occurs
        if (getPanels(modeNew) != getPanels(modeOld)) edgeCache = 0;

        if (modeNew != modeOld || force) { // must rebuild on new game, because buffers are changing
            dimSpaceCache = dimSpace;
            enableCache = os.enable;
            rebuildDisplay(scale, os);
        } else {
            for (Display value : display) value.setOptions(scale, os);
        }
    }

    /**
     * A function that rebuilds the display objects, for when the stereo mode has changed.
     */
    private void rebuildDisplay(double scale, OptionsStereo os) {
        switch (getDisplayMode(dimSpaceCache, os.enable)) {

            case DISPLAY_MODE_3D:

                display = new Display[1];
                display[0] = new DisplayScaled(bufRelative, bufDisplay[0], scale);

                displayInterface.setMode3D(bufDisplay[0]);
                break;

            case DISPLAY_MODE_4D_MONO:

                display = new Display[1];
                display[0] = new DisplayStereo(bufRelative, bufDisplay[0], 0, scale, os, edgeCache);

                displayInterface.setMode4DMono(bufDisplay[0]);
                break;

            case DISPLAY_MODE_4D_STEREO:

                display = new Display[2];
                display[0] = new DisplayStereo(bufRelative, bufDisplay[0], -1, scale, os, edgeCache);
                display[1] = new DisplayStereo(bufRelative, bufDisplay[1], +1, scale, os, edgeCache);

                displayInterface.setMode4DStereo(bufDisplay[0], bufDisplay[1]);
                break;
        }
    }

    public boolean canMove(int a, double d) {

        Vec.addScaled(reg3, origin, axis[a], d);
        return Grid.isOpenMove(origin, reg3, map, reg1, reg4);
    }

// --- fixed objects ---

    private boolean atFinish() {

        int dir = Grid.toCell(reg1, reg2, origin);
        return (Grid.equals(reg1, map.getFinish())
                || (dir != Direction.DIR_NONE && Grid.equals(reg2, map.getFinish())));
    }

    public void move(int a, double d) {

        // make the move, if allowed

        Vec.addScaled(reg3, origin, axis[a], d);
        if (!Grid.isOpenMove(origin, reg3, map, reg1, reg4)) return;
        Vec.copy(origin, reg3);

        // check for finish

        if (atFinish()) {
            win = true;
        }
    }

    public void rotateAngle(int a1, int a2, double theta) {
        Vec.rotateAngle(axis[a1], axis[a2], axis[a1], axis[a2], theta);
    }

    public Align align() {
        return new Align(origin, axis);
    }

    public void renderAbsolute() {
        renderAbsolute.run(origin);
        renderRelative();
    }

    private void renderObject(LineBuffer buf, double[][] obj) {
        for (int i = 0; i < obj.length; i += 2) {
            buf.add(obj[i], obj[i + 1], Color.white);
        }
    }

    private void renderRelative() {
        renderRelative.run(axis);

        renderObject(bufRelative, objRetina);
        renderObject(bufRelative, objCross);
        if (win) renderObject(bufRelative, objWin);

        renderDisplay();
    }

    private void renderDisplay() {
        for (Display value : display) {
            value.run();
        }
        displayInterface.nextFrame();
    }

}

