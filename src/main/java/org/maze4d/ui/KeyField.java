/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.ui.dialog.DialogKey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A custom field for entering key codes and modifiers.
 */
public class KeyField extends JTextField {

    public static Dialog owner;

    private Key key;

    public KeyField(int columns) {
        this(columns, true);
    }

    public KeyField(int columns, boolean enabled) {
        super(columns);
        setEnabled(false);

        if (enabled) addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                doClick();
            }
        });
    }

// --- key dialog ---

    private void doClick() {
        Key result = new DialogKey(owner).run();
        if (result != null) {
            put(result);
        }
    }

    public void get(Key key) {
        key.code = this.key.code;
        key.modifiers = this.key.modifiers;
    }

    public void put(Key key) {
        this.key = key;
        Field.putString(this, key.toString());
    }

}

