/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.display;

import org.maze4d.options.OptionsStereo;
import org.maze4d.utils.Line;
import org.maze4d.utils.LineBuffer;
import org.maze4d.utils.Vec;

/**
 * An object that displays a two-dimensional retina in scaled form.
 */
public class DisplayScaled implements Display {

    private final LineBuffer in;
    private final LineBuffer out;

    private double scale;

    public DisplayScaled(LineBuffer in, LineBuffer out, double scale) {
        this.in = in;
        this.out = out;

        this.scale = scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    @Override
    public void setScreenWidth(double screenWidth) {

    }

    @Override
    public void setScreenDistance(double screenDistance) {

    }

    @Override
    public void setEyeSpacing(double eyeSpacing) {

    }

    @Override
    public void setTiltVertical(double tiltVertical) {

    }

    @Override
    public void setTiltHorizontal(double tiltHorizontal) {

    }

    public void setOptions(double scale, OptionsStereo os) {
        this.scale = scale;
        // the other arguments don't matter here
    }

    @Override
    public void setEdge(int edge) {

    }

// --- processing ---

    private void convert(double[] dest, double[] src) {
        Vec.scale(dest, src, scale);
    }

    public void run() {
        out.clear();
        for (int i = 0; i < in.size(); i++) {
            Line src = in.get(i);
            Line dest = out.getNext();

            convert(dest.getP1(), src.getP1());
            convert(dest.getP2(), src.getP2());
            dest.setColor(src.getColor());
        }
    }

}

