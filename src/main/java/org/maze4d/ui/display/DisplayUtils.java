/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.display;

import org.maze4d.utils.Permute;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;

/**
 * Utility class for everything UI related that doesn't fit anywhere else.
 */
public final class DisplayUtils {

    private DisplayUtils() {
    }

    public static Rectangle getCenteredRectangle(int width, int height) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        if (width > screenSize.width) width = screenSize.width;
        if (height > screenSize.height) height = screenSize.height;

        int x = (screenSize.width - width) / 2;
        int y = (screenSize.height - height) / 2;

        return new Rectangle(x, y, width, height);
    }

    public static boolean isModifier(int code) {

        // I thought there was some predefined isModifier function, but I can't find it.
        // so, these are all the key-based modifiers defined in java.awt.event.InputEvent,
        // plus all the lock keys, just because.

        return (code == KeyEvent.VK_SHIFT
                || code == KeyEvent.VK_CONTROL
                || code == KeyEvent.VK_META
                || code == KeyEvent.VK_ALT

                || code == KeyEvent.VK_ALT_GRAPH

                || code == KeyEvent.VK_CAPS_LOCK
                || code == KeyEvent.VK_NUM_LOCK
                || code == KeyEvent.VK_SCROLL_LOCK);
    }

    public static int[] getAllowedModifiers() {
        return new int[]{KeyEvent.SHIFT_MASK, // TODO replacing SHIFT_MASK with SHIFT_DOWN_MASK lets the app not start
                KeyEvent.CTRL_MASK,
                KeyEvent.META_MASK,
                KeyEvent.ALT_MASK};
    }

    public static int restrictToAllowedModifiers(int modifiers) {
        final int mask = KeyEvent.SHIFT_MASK
                | KeyEvent.CTRL_MASK
                | KeyEvent.META_MASK
                | KeyEvent.ALT_MASK;
        return modifiers & mask;
    }

    public static int translateAllowedModifier(int code) {
        int modifier = 0;
        switch (code) {
            case KeyEvent.VK_SHIFT:
                modifier = KeyEvent.SHIFT_MASK;
                break;
            case KeyEvent.VK_CONTROL:
                modifier = KeyEvent.CTRL_MASK;
                break;
            case KeyEvent.VK_META:
                modifier = KeyEvent.META_MASK;
                break;
            case KeyEvent.VK_ALT:
                modifier = KeyEvent.ALT_MASK;
                break;
        }
        return modifier;
    }

    public static void generate(Color[] bySomething, Color[] colors, Random random) {

        int[] p = Permute.permute(bySomething.length, colors.length, random);

        for (int i = 0; i < bySomething.length; i++) {
            bySomething[i] = colors[p[i]];
        }
    }
}
