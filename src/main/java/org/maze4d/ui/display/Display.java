/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.display;

import org.maze4d.options.OptionsStereo;

/*
 * A common interface for display objects.
 */
public interface Display {

    void run();

    void setScale(double scale);

    void setScreenWidth(double screenWidth);

    void setScreenDistance(double screenDistance);

    void setEyeSpacing(double eyeSpacing);

    void setTiltVertical(double tiltVertical);

    void setTiltHorizontal(double tiltHorizontal);

    void setOptions(double scale, OptionsStereo os);

    void setEdge(int edge);

}

