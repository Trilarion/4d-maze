/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

/**
 * An interface for invoking menu commands via keyboard shortcuts.
 */
public interface Menu {

    void doNew();

    void doOptions();

    void doExit();

}

