/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.ValidationException;
import org.maze4d.model.Core;
import org.maze4d.options.Options;
import org.maze4d.options.OptionsAll;
import org.maze4d.property.PropertyFile;
import org.maze4d.property.PropertyResource;
import org.maze4d.property.Storable;
import org.maze4d.property.Store;
import org.maze4d.ui.dialog.DialogAbout;
import org.maze4d.ui.dialog.DialogOptions;
import org.maze4d.ui.dialog.DialogWelcome;
import org.maze4d.ui.display.DisplayUtils;
import org.maze4d.ui.panel.PanelLine;
import org.maze4d.ui.utils.ImageFileFilter;
import org.maze4d.ui.utils.SquareLayout;
import org.maze4d.utils.App;
import org.maze4d.utils.LineBuffer;
import org.maze4d.utils.PostScriptFile;
import org.maze4d.utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

/**
 * The entry point and main frame window for the maze application.
 */
public class MainFrame extends JFrame implements Observer, IDisplay, Menu, Storable {

    private static final String KEY_OPTIONS = "opt";

    private static final String KEY_DIM = "dim";
    private static final String KEY_BOUNDS = "bounds";
    private static final String KEY_GAME_DIRECTORY = "dir.game";
    private static final String KEY_IMAGE_DIRECTORY = "dir.image";
    private static final String nameDefault = "default.properties";
    private static final File fileCurrent = new File("current.properties");
    private final SquareLayout layout;
    private final LineBuffer[] lineBuffer;
    private final PanelLine[] panelLine;
    private final Options optDefault;
    public Options opt; // the next three are used only during load
    public int dim;
    public Rectangle bounds;
    public final Core core;
    private int active;
    private File gameDirectory;
    private File imageDirectory;
    private DialogOptions dialogOptions;


    /**
     * Construct a new main frame window without making it visible.
     */
    public MainFrame() {
        super(App.getString("Maze.s1"));

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                doExit();
            }
        });

        setJMenuBar(constructMenuBar());

        Container contentPane = getContentPane();
        contentPane.setLayout(layout = new SquareLayout(this));
        contentPane.setBackground(Color.black);

        // field initialization

        lineBuffer = new LineBuffer[2];
        // null until core callback

        panelLine = new PanelLine[2];
        for (int i = 0; i < 2; i++) {
            panelLine[i] = new PanelLine();
            contentPane.add(panelLine[i]);
        }

        // active is zero until core callback

        optDefault = new Options();
        opt = new Options();
        bounds = new Rectangle();

        // dialogOptions is null until used
        // rest is handled in construct, after properties loaded

        boolean result = true;
        try {
            PropertyResource.load(nameDefault, new Storable() {
                public void load(Store store) throws ValidationException {
                    loadDefault(store);
                }

                public void save(Store store) {
                }
            });
            if (fileCurrent.exists()) PropertyFile.load(fileCurrent, this);
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
            result = false;
        }
        if (!result) {
            System.exit(1);
        } // must exit to stop event dispatching
        if (!fileCurrent.exists()) doWelcomeStartup();

        // here we don't have to be careful about modifying an existing object,
        // because if any of the load process fails, the program will exit

        core = new Core(opt, this, this);
        addKeyListener(core);

        core.newGame(dim);

        setBounds(bounds); // do not pack, size is not controlled by components

        opt = null; // done with these (and with dim)
        bounds = null;
    }

    public void update(int edge) {
        core.setEdge(edge);
    }

    public void setMode3D(LineBuffer buf) {
        lineBuffer[0] = buf;
        lineBuffer[1] = null;
        panelLine[0].setVisible(true);
        panelLine[1].setVisible(false);
        active = 1;
    }

    public void setMode4DMono(LineBuffer buf) {
        lineBuffer[0] = buf;
        lineBuffer[1] = null;
        panelLine[0].setVisible(true);
        panelLine[1].setVisible(false);
        active = 1;
    }

    public void setMode4DStereo(LineBuffer buf1, LineBuffer buf2) {
        lineBuffer[0] = buf1;
        lineBuffer[1] = buf2;
        panelLine[0].setVisible(true);
        panelLine[1].setVisible(true);
        active = 2;
    }

// --- image files ---

    public void nextFrame() {
        for (int i = 0; i < active; i++) {
            panelLine[i].setLines(lineBuffer[i]);
        }
    }

    public void loadDefault(Store store) throws ValidationException {
        store.getObject(KEY_OPTIONS, optDefault);

        if (fileCurrent.exists()) return;

        store.getObject(KEY_OPTIONS, opt);
        dim = 4;
        bounds = DisplayUtils.getCenteredRectangle(500, 300); // nice reasonable size
        gameDirectory = null;
        imageDirectory = null;
    }

    public void load(Store store) throws ValidationException {

        store.getObject(KEY_OPTIONS, opt);
        dim = store.getInteger(KEY_DIM);
        if (!(dim == 3 || dim == 4)) throw App.getException("Maze.e1");
        store.getObject(KEY_BOUNDS, bounds);
        gameDirectory = Utils.toFile(store.getString(KEY_GAME_DIRECTORY));
        imageDirectory = Utils.toFile(store.getString(KEY_IMAGE_DIRECTORY));
    }

    public void save(Store store) throws ValidationException {
        store.putObject(KEY_OPTIONS, core.getOptions());
        store.putInteger(KEY_DIM, core.getDim());
        store.putObject(KEY_BOUNDS, getBounds());
        store.putString(KEY_GAME_DIRECTORY, Utils.toString(gameDirectory));
        store.putString(KEY_IMAGE_DIRECTORY, Utils.toString(imageDirectory));
    }

// --- menu commands ---

    public void doWelcomeStartup() {
        DialogWelcome dialog = new DialogWelcome(this);

        dialog.put(opt.ok4, opt.os, opt.oi);
        dialog.setVisible(true);

        opt.os = dialog.os;
        opt.oi = dialog.oi;
    }

    private void doWelcome() {
        DialogWelcome dialog = new DialogWelcome(this);

        Options opt = core.getOptions();
        dialog.put(opt.ok4, opt.os, opt.oi);
        dialog.setVisible(true);

        core.setOptions(dialog.os, dialog.oi);
    }

    private void doAbout() {
        DialogAbout dialog = new DialogAbout(this);
        dialog.setVisible(true);
    }

    public void doNew() {
        core.newGame(0);
    }

    private void doNew3D() {
        core.newGame(3);
    }

    private void doNew4D() {
        core.newGame(4);
    }

    private File getGameDirectory() {
        return (gameDirectory != null) ? gameDirectory : imageDirectory;
    }

    private File getImageDirectory() {
        return (imageDirectory != null) ? imageDirectory : gameDirectory;
    }

    private boolean confirmOverwrite(File file, String title) {
        String message = App.getString("Maze.s21", new Object[]{file.getName()});
        int result = JOptionPane.showConfirmDialog(this, message, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        return (result == JOptionPane.OK_OPTION);
    }

    private void doLoad() {

        JFileChooser chooser = new JFileChooser(getGameDirectory());
        chooser.setDialogTitle(App.getString("Maze.s14"));
        int result = chooser.showOpenDialog(this);
        if (result != JFileChooser.APPROVE_OPTION) return;

        gameDirectory = chooser.getCurrentDirectory();
        File file = chooser.getSelectedFile();

        try {
            PropertyFile.load(file, core);
        } catch (ValidationException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), App.getString("Maze.s15"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void doSave() {

        JFileChooser chooser = new JFileChooser(getGameDirectory());
        String title = App.getString("Maze.s16");
        chooser.setDialogTitle(title);
        int result = chooser.showSaveDialog(this);
        if (result != JFileChooser.APPROVE_OPTION) return;

        gameDirectory = chooser.getCurrentDirectory();
        File file = chooser.getSelectedFile();
        if (file.exists() && !confirmOverwrite(file, title)) return;

        try {
            PropertyFile.save(file, core);
        } catch (ValidationException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), App.getString("Maze.s17"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void doSaveImage() {

        JFileChooser chooser = new JFileChooser(getImageDirectory());
        String title = App.getString("Maze.s18");
        chooser.setDialogTitle(title);
        chooser.setFileFilter(new ImageFileFilter());
        int result = chooser.showSaveDialog(this);
        if (result != JFileChooser.APPROVE_OPTION) return;

        imageDirectory = chooser.getCurrentDirectory();
        File file = Utils.forceSuffix(chooser.getSelectedFile());
        if (file.exists() && !confirmOverwrite(file, title)) return;

        try {
            PostScriptFile.save(file, active, lineBuffer,
                    layout.getEdge(), layout.getGap(), core.os().screenWidth, core.oi());
        } catch (ValidationException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), App.getString("Maze.s19"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void doResetWin() {
        core.resetWin();
    }

    private void doRestart() {
        core.restartGame();
    }

    public void doOptions() {
        if (dialogOptions == null) {
            dialogOptions = new DialogOptions(this, optDefault);
        }
        dialogOptions.prepare(core.getDim(), core.getOptionsAll());
        dialogOptions.setVisible(true);

        OptionsAll oaResult = dialogOptions.getResult();
        if (oaResult != null) core.setOptionsAll(oaResult);
        else core.hack(); // fix key mapper problem
    }

    public void doExit() {
        try {
            PropertyFile.save(fileCurrent, this);
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
        System.exit(0);
    }

// --- UI helpers ---

    private JMenuBar constructMenuBar() {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        JMenuItem item;

        menu = new JMenu(App.getString("Maze.s2"));
        menuBar.add(menu);

        item = new JMenuItem(App.getString("Maze.s22"));
        item.addActionListener(e -> doWelcome());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s10"));
        item.addActionListener(e -> doAbout());
        menu.add(item);

        menu.addSeparator();

        item = new JMenuItem(App.getString("Maze.s3"));
        item.addActionListener(e -> doNew3D());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s4"));
        item.addActionListener(e -> doNew4D());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s5"));
        item.addActionListener(e -> doLoad());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s6"));
        item.addActionListener(e -> doSave());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s11"));
        item.addActionListener(e -> doSaveImage());
        menu.add(item);

        menu.addSeparator();

        item = new JMenuItem(App.getString("Maze.s7"));
        item.addActionListener(e -> doResetWin());
        menu.add(item);

        item = new JMenuItem(App.getString("Maze.s8"));
        item.addActionListener(e -> doRestart());
        menu.add(item);

        menu.addSeparator();

        item = new JMenuItem(App.getString("Maze.s9"));
        item.addActionListener(e -> doOptions());
        menu.add(item);

        menu.addSeparator();

        item = new JMenuItem(App.getString("Maze.s12"));
        item.addActionListener(e -> doExit());
        menu.add(item);

        return menuBar;
    }

}

