/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.options.OptionsColor;
import org.maze4d.ui.display.DisplayUtils;
import org.maze4d.utils.*;

import java.awt.*;
import java.util.Random;

/**
 * An object that determines face colors using one of several methods.
 * The different methods are implemented in a single object
 * so that you can switch back and forth without rebuilding the (large) color array.
 */
public class Colorizer implements Colorize {

    private final int dimSpace;
    private final int dimMap;
    private final int[] limits;
    private final ColorDynamicArray byCell;
    private final Color[] byOrientation;
    private final Color[] byDirection;
    private final OptionsColor ocCache; // cache only used to detect changes
    private int colorMode;
    private long seedCache;

    public Colorizer(int dimSpace, int dimMap, int[] size, OptionsColor oc, long seed) {
        this.dimSpace = dimSpace;
        this.dimMap = dimMap;
        limits = DynamicArray.makeLimits(size);

        byCell = new ColorDynamicArray(dimSpace, limits);
        byOrientation = new Color[dimSpace];
        byDirection = new Color[2 * dimSpace];

        ocCache = new OptionsColor();

        setOptions(oc, seed, true);
    }

    public void setColorMode(int colorMode) {
        this.colorMode = ocCache.colorMode = colorMode;
    }

    public void setOptions(OptionsColor oc, long seed) {
        setOptions(oc, seed, false);
    }

// --- by cell ---

    private void setOptions(OptionsColor oc, long seed, boolean force) {

        // changing anything but the color mode requires full generation

        colorMode = ocCache.colorMode = oc.colorMode;
        if (!force
                && OptionsColor.equals(ocCache, oc)
                && seedCache == seed) return;

        OptionsColor.copy(ocCache, oc);
        seedCache = seed;

        generate(oc, seed);
    }

    private void generate(OptionsColor oc, long seed) {

        Color[] colors = oc.getColors();
        Random random = new Random(seed);

        generateByCell(oc.dimSameParallel, oc.dimSamePerpendicular, colors, random);

        DisplayUtils.generate(byOrientation, colors, random);
        DisplayUtils.generate(byDirection, colors, random);
    }

// --- by orientation and direction ---

    private void generateByCell(int dimSameParallel, int dimSamePerpendicular, Color[] colors, Random random) {

        // figure out the dimension numbers

        int dimNon = dimSpace - dimMap;

        // the same-color dimensions are not checked against any upper bound ... until now
        int dimMapSame = Math.min(dimSameParallel, dimMap);
        int dimNonSame = Math.min(dimSamePerpendicular, dimNon);

        // define these for slightly easier reading
        int dimMapDiff = dimMap - dimMapSame;
        int dimNonDiff = dimNon - dimNonSame;

        // decide which dimensions are which

        // make lists of the map and nonmap dimensions,
        // then permute them to decide which ones will be the same-color dimensions

        int[] aMap = Permute.sequence(0, dimMap);
        int[] aNon = Permute.sequence(dimMap, dimNon);

        Permute.permute(aMap, random);
        Permute.permute(aNon, random);

        int[] aSame = Utils.join(aMap, 0, dimMapSame, aNon, 0, dimNonSame);
        int[] aDiff = Utils.join(aMap, dimMapSame, dimMapDiff, aNon, dimNonSame, dimNonDiff);

        // now fill in the array

        for (DynamicArrayIterator iDiff = new DynamicArrayIterator(aDiff, new int[dimSpace], limits);
             iDiff.hasCurrent(); iDiff.increment()) {

            Color color = colors[random.nextInt(colors.length)];

            for (DynamicArrayIterator iSame = new DynamicArrayIterator(aSame, iDiff.current(), limits);
                 iSame.hasCurrent(); iSame.increment()) {

                byCell.set(iSame.current(), color);
            }
        }
    }


    public Color getColor(int[] p, int dir) {
        Color color;

        switch (colorMode) {

            case OptionsColor.COLOR_MODE_EXTERIOR:
                Direction.apply(dir, p, 1);
                color = byCell.get(p);
                Direction.apply(dir, p, -1);
                break;

            case OptionsColor.COLOR_MODE_INTERIOR:
                color = byCell.get(p);
                break;

            case OptionsColor.COLOR_MODE_BY_ORIENTATION:
                color = byOrientation[Direction.getAxis(dir)];
                break;

            case OptionsColor.COLOR_MODE_BY_DIRECTION:
                color = byDirection[dir];
                break;

            default:
                throw new IllegalArgumentException();
        }

        return color;
    }

}

