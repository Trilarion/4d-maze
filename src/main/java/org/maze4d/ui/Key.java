/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.ValidationException;
import org.maze4d.ui.display.DisplayUtils;
import org.maze4d.utils.App;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * An object that represents a key that can be pressed.
 * The class is similar to javax.swing.KeyStroke for a key press,
 * but it avoids the possibility of having other kinds of events,
 * and also works with org.maze4d.property.PropertyStore persistence.
 */
public class Key implements Validate {

    public int code;
    public int modifiers;

    public Key() {
        this(0, 0);
    }

    public Key(int code) {
        this(code, 0);
    }

    public Key(int code, int modifiers) {
        this.code = code;
        this.modifiers = modifiers;
    }


    public boolean isDefined() {
        return !(code == 0 && modifiers == 0);
    }

    public boolean equals(Key key) {
        return (code == key.code
                && modifiers == key.modifiers);
    }

    public String toString() {
        String s = "";
        if (isDefined()) {
            s = InputEvent.getModifiersExText(modifiers);
            if (!s.isEmpty()) s += " ";
            s += KeyEvent.getKeyText(code);
        }
        return s;
    }

    public void validate() throws ValidationException {
        // this first line isn't necessary right now
        // if ( ! isDefined() ) return;
        if (modifiers != DisplayUtils.restrictToAllowedModifiers(modifiers)) throw App.getException("Key.e1");
    }

}

