/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui;

import org.maze4d.utils.Line;
import org.maze4d.utils.LineBuffer;
import org.maze4d.utils.Vec;

/**
 * An object that takes a set of lines oriented with respect to absolute coordinates
 * and converts them to relative coordinates, i.e., projects them onto a retina.
 */
public class RenderRelative {

    private final LineBuffer in;
    private final LineBuffer out;
    private final int dim;
    private final double[][] clip;
    private final double[] reg1; // temporary registers
    private final double[] reg2;
    private double retina;

    public RenderRelative(LineBuffer in, LineBuffer out, int dim, double retina) {
        this.in = in;
        this.out = out;
        this.dim = dim;

        clip = new double[2 * (dim - 1)][dim];
        reg1 = new double[dim];
        reg2 = new double[dim];

        setRetina(retina);
    }

    public void setRetina(double retina) {
        this.retina = retina;

        int next = 0;
        for (int a = 0; a < dim - 1; a++) {

            clip[next][a] = 1;
            clip[next][dim - 1] = retina;
            next++;

            clip[next][a] = -1;
            clip[next][dim - 1] = retina;
            next++;

            // no need to zero other components,
            // they never become nonzero
        }
    }

// --- processing ---

    // the call to projectRetina could cause division by zero
    // if the line being projected started or ended on the parallel plane through the origin.
    // that's not a problem here.
    // the clipping planes restrict the lines to a forward cone,
    // so the origin is the only dangerous point,
    // and we know there are no lines through the origin
    // because we're not allowed to move onto the walls.

    private boolean convert(Line dest, Line src, double[][] axis) {

        Vec.toAxisCoordinates(reg1, src.getP1(), axis);
        Vec.toAxisCoordinates(reg2, src.getP2(), axis);

        for (double[] doubles : clip) {
            if (Vec.clip(reg1, reg2, doubles)) return false;
        }

        Vec.projectRetina(dest.getP1(), reg1, retina);
        Vec.projectRetina(dest.getP2(), reg2, retina);
        dest.setColor(src.getColor());

        return true;
    }

    public void run(double[][] axis) {
        out.clear();
        for (int i = 0; i < in.size(); i++) {
            Line src = in.get(i);
            Line dest = out.getNext();

            if (!convert(dest, src, axis)) out.unget();
        }
    }

}

