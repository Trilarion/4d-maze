/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.panel;

import org.maze4d.ValidationException;
import org.maze4d.options.Options;

/**
 * An abstract panel superclass for editing options.
 */
public interface PanelOptions {

    /**
     * Get the options from the UI (into an existing object).
     */
    void get(Options opt) throws ValidationException;

    /**
     * Put the options into the UI.
     */
    void put(Options opt);

}

