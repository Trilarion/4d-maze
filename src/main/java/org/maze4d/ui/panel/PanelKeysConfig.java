/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.panel;

import org.maze4d.ValidationException;
import org.maze4d.options.Options;
import org.maze4d.options.OptionsKeysConfig;
import org.maze4d.ui.KeyField;
import org.maze4d.utils.App;

import javax.swing.*;

/**
 * A panel superclass for editing keys options.
 */
public class PanelKeysConfig extends JPanel implements PanelOptions {

    protected final KeyField[] key;
    private final int[] index;

    public PanelKeysConfig(int[] index) {
        this.index = index;

        key = new KeyField[index.length];

        for (int j = 0; j < index.length; j++) {
            key[j] = new KeyField(7);
        }
    }

    protected static JLabel label(String key) {
        return label(key, true);
    }

    protected static JLabel label(String key, boolean pad) {
        String s = App.getString("PanelKeys." + key);
        if (pad) s += " ";
        return new JLabel(s);
    }

    protected void get(OptionsKeysConfig okc) throws ValidationException {
        for (int j = 0; j < index.length; j++) {
            key[j].get(okc.key[index[j]]);
        }

        // multi-tab options validated in org.maze4d.ui.dialog.DialogOptions
    }

    public void get(Options opt) throws ValidationException {
        get(opt.okc);
    }

    protected void put(OptionsKeysConfig okc) {
        for (int j = 0; j < index.length; j++) {
            key[j].put(okc.key[index[j]]);
        }
    }

    public void put(Options opt) {
        put(opt.okc);
    }

}

