/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.ui.panel;

import org.maze4d.ValidationException;
import org.maze4d.options.OptionsKeysConfig;
import org.maze4d.ui.Field;
import org.maze4d.ui.utils.GridBagHelper;
import org.maze4d.utils.App;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A panel for editing keys options.
 */
public class PanelKeysParam extends PanelKeysConfig {

    private static final int[] myindex = {OptionsKeysConfig.KEY_PARAM_DECREASE + 0,
            OptionsKeysConfig.KEY_PARAM_DECREASE + 1,
            OptionsKeysConfig.KEY_PARAM_DECREASE + 2,
            OptionsKeysConfig.KEY_PARAM_DECREASE + 3,
            OptionsKeysConfig.KEY_PARAM_DECREASE + 4,
            OptionsKeysConfig.KEY_PARAM_DECREASE + 5,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 0,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 1,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 2,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 3,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 4,
            OptionsKeysConfig.KEY_PARAM_INCREASE + 5};

    // --- constants ---
    private static final String[] paramNames = {"",
            App.getString("PanelKeys.s32"),
            App.getString("PanelKeys.s33"),
            App.getString("PanelKeys.s34"),
            App.getString("PanelKeys.s35"),
            App.getString("PanelKeys.s36"),
            App.getString("PanelKeys.s37"),
            App.getString("PanelKeys.s38"),
            App.getString("PanelKeys.s39"),
            App.getString("PanelKeys.s40")};
    private static final int[] paramValues = {OptionsKeysConfig.PARAM_NONE,
            OptionsKeysConfig.PARAM_COLOR_MODE,
            OptionsKeysConfig.PARAM_DEPTH,
            OptionsKeysConfig.PARAM_RETINA,
            OptionsKeysConfig.PARAM_SCALE,
            OptionsKeysConfig.PARAM_SCREEN_WIDTH,
            OptionsKeysConfig.PARAM_SCREEN_DISTANCE,
            OptionsKeysConfig.PARAM_EYE_SPACING,
            OptionsKeysConfig.PARAM_TILT_VERTICAL,
            OptionsKeysConfig.PARAM_TILT_HORIZONTAL};
    private final List<JComboBox<String>> param;

    public PanelKeysParam() {
        super(myindex);

        param = new ArrayList<>(6);

        for (int i = 0; i < 6; i++) {
            param.add(new JComboBox<>(paramNames));
            param.get(i).setMaximumRowCount(paramNames.length); // avoid scrolling
        }

        GridBagHelper helper = new GridBagHelper(this);

        helper.addCenter(2, 0, label("s29"));
        helper.addCenter(4, 0, label("s30"));

        helper.add(0, 1, label("s31"));

        for (int i = 0; i < 6; i++) {
            helper.add(1, 1 + i, param.get(i));
        }

        for (int j = 0; j < 12; j++) {
            int dx = 2 * (j / 6);
            int dy = j % 6;
            helper.add(2 + dx, 1 + dy, key[j]);
        }

        helper.addBlank(0, 7);
        helper.addBlank(5, 0);

        helper.setRowWeight(7, 1);
        helper.setColumnWeight(1, 1);
        helper.setColumnWeight(3, 1);
        helper.setColumnWeight(5, 1);
    }

    protected void get(OptionsKeysConfig okc) throws ValidationException {
        for (int i = 0; i < 6; i++) {
            okc.param[i] = Field.getEnumerated(param.get(i), paramValues);
        }
        super.get(okc);
    }

    protected void put(OptionsKeysConfig okc) {
        for (int i = 0; i < 6; i++) {
            Field.putEnumerated(param.get(i), paramValues, okc.param[i]);
        }
        super.put(okc);
    }

}

