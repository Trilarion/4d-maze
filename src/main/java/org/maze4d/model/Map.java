/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

import org.maze4d.options.OptionsMap;
import org.maze4d.utils.BooleanDynamicArray;
import org.maze4d.utils.DynamicArray;

/**
 * An object that contains map data.
 */
public class Map {

    private final BooleanDynamicArray map;
    private int[] start;
    private int[] finish;

// --- accessors ---

    public Map(int dimSpace, OptionsMap om, long seed) {

        int[] limits = DynamicArray.makeLimits(om.size);

        map = new BooleanDynamicArray(dimSpace, limits);
        // elements start out false, which is correct

        // start and finish are produced by the generation algorithm

        new MapGenerator(this, limits, om, seed).generate();
    }

    public boolean inBounds(int[] p) {
        return map.inBounds(p);
    }

    public boolean isOpen(int[] p) {
        return map.get(p);
    }

    public void setOpen(int[] p, boolean b) {
        map.set(p, b);
    } // generator only

    public int[] getStart() {
        return start;
    }

    public void setStart(int[] start) {
        this.start = start;
    } // generator only

    public int[] getFinish() {
        return finish;
    }

    public void setFinish(int[] finish) {
        this.finish = finish;
    }

}

