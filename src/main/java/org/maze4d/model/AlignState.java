/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

public enum AlignState {
    TRANSLATE(0), ROTATE(1), COMPLETE(2);
    private final int value;

    AlignState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}