/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

import org.maze4d.ValidationException;
import org.maze4d.controller.Controller;
import org.maze4d.options.*;
import org.maze4d.property.Storable;
import org.maze4d.property.Store;
import org.maze4d.ui.Engine;
import org.maze4d.ui.IDisplay;
import org.maze4d.ui.Menu;
import org.maze4d.ui.utils.Clock;
import org.maze4d.utils.App;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * The game core, mostly detached from the user interface.
 */
public class Core implements IOptions, Storable, KeyListener {

    private static final String VALUE_CHECK = "Maze";
    private static final String KEY_CHECK = "game";
    private static final String KEY_DIM = "dim";
    private static final String KEY_OPTIONS_MAP = "om";
    private static final String KEY_OPTIONS_COLOR = "oc";
    private static final String KEY_OPTIONS_VIEW = "ov";
    private static final String KEY_OPTIONS_SEED = "oe";
    private static final String KEY_ALIGN_MODE = "align";
    private final OptionsAll oa;
    private final Engine engine;
    private final KeyBuffer keyBuffer;
    private final KeyMapper keyMapper3;
    private final KeyMapper keyMapper4;
    private final Controller controller;
    private final Clock clock;
    // some of these also implement org.maze4d.options.IOptions
    private int dim;

    public Core(Options opt, IDisplay displayInterface, Menu menu) {

        // dim and rest of oa are initialized when new game started

        oa = new OptionsAll();
        oa.opt = opt;
        oa.omCurrent = new OptionsMap(0); // blank for copying into
        oa.oeNext = new OptionsSeed();

        engine = new Engine(displayInterface);

        keyBuffer = new KeyBuffer();
        keyMapper3 = new KeyMapper(keyBuffer, opt.ok3, opt.okc);
        keyMapper4 = new KeyMapper(keyBuffer, opt.ok4, opt.okc);
        controller = new Controller(keyBuffer, this, menu, engine);
        clock = new Clock(controller);
    }

// --- external interface ---

    private OptionsMap om() {
        // omCurrent is always non-null, so can be used directly
        return (dim == 3) ? oa.opt.om3 : oa.opt.om4;
    }

    public OptionsColor oc() {
        if (oa.ocCurrent != null) return oa.ocCurrent;
        return (dim == 3) ? oa.opt.oc3 : oa.opt.oc4;
    }

    public OptionsView ov() {
        if (oa.ovCurrent != null) return oa.ovCurrent;
        return (dim == 3) ? oa.opt.ov3 : oa.opt.ov4;
    }

    public OptionsStereo os() {
        return oa.opt.os;
    }

    private OptionsKeys ok() {
        return (dim == 3) ? oa.opt.ok3 : oa.opt.ok4;
    }

    private OptionsMotion ot() {
        return (dim == 3) ? oa.opt.ot3 : oa.opt.ot4;
    }

    public OptionsImage oi() {
        return oa.opt.oi;
    }

    private KeyMapper keyMapper() {
        return (dim == 3) ? keyMapper3 : keyMapper4;
    }

    public void newGame(int dim) {
        if (dim != 0) this.dim = dim; // allow zero to mean "keep the same"

        OptionsMap.copy(oa.omCurrent, om());
        oa.ocCurrent = null; // use standard colors for dimension
        oa.ovCurrent = null; // ditto
        oa.oeCurrent = oa.oeNext;
        oa.oeCurrent.forceSpecified();
        oa.oeNext = new OptionsSeed();

        engine.newGame(this.dim, oa.omCurrent, oc(), ov(), oa.opt.os, oa.oeCurrent, true);

        controller.setOptions(oa.opt.okc, ot());
        clock.setFrameRate(ot().frameRate);

        boolean hack = keyBuffer.down[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_NEW_GAME)];
        keyMapper().releaseAll(); // sync up key mapper, which may have changed with dim
        if (hack) keyMapper().unrelease(oa.opt.okc.key[OptionsKeysConfig.KEY_NEW_GAME]);

        // the purpose of the above hack is to prevent auto-repeat of the new-game key.
        // normally the key mapper prevents auto-repeat by seeing that the key is already down,
        // but here in newGame we release the keys, so the next auto-repeat counts as a press.
        //
        // it is important that the new-game key does not change between mappers,
        // otherwise we would be putting the mapper into a factually incorrect state.
        //
        // a better solution would be to track the state of all the keys,
        // not just ones that are mapped, then we wouldn't need to release the keys,
        // we could just transfer the state from one mapper to the other.

        controller.reset(this.dim, ok().startAlignMode);
        // clock will stop when controller reports idle
    }

    public void hack() {
        boolean hack = keyBuffer.down[KeyBuffer.getKeyConfigID(OptionsKeysConfig.KEY_OPTIONS)];
        if (hack) keyMapper().release(oa.opt.okc.key[OptionsKeysConfig.KEY_OPTIONS]);

        // another key mapper hack.  when the options dialog comes up,
        // the main window loses focus, and we don't receive a key-released event.
        // if setOptions is called, that resets the mapper and fixes it,
        // but if the dialog is closed or canceled, there's a problem.
    }


    public void resetWin() {
        engine.resetWin();
    }

    public void restartGame() {
        engine.restartGame();

        keyMapper().releaseAll(); // sync up key mapper, which may have changed with dim
        controller.reset(dim, ok().startAlignMode);
        // clock will stop when controller reports idle
    }

    public int getDim() {
        return dim;
    }

    /**
     * Get options.  Ownership is not transferred, so the object should not be modified.
     */
    public Options getOptions() {
        return oa.opt;
    }

    /**
     * Get options.  Ownership is not transferred, so the object should not be modified.
     */
    public OptionsAll getOptionsAll() {
        return oa;
    }

    /**
     * Set options.  Ownership is transferred.
     */
    public void setOptionsAll(OptionsAll oa) {
        this.oa.opt = oa.opt;
        // omCurrent is read-only
        this.oa.ocCurrent = oa.ocCurrent; // nullity should remain the same
        this.oa.ovCurrent = oa.ovCurrent; // ditto
        this.oa.oeCurrent = oa.oeCurrent;
        this.oa.oeCurrent.forceSpecified(); // make blanks into new random seeds
        this.oa.oeNext = oa.oeNext;

        engine.setOptions(oc(), ov(), this.oa.opt.os, this.oa.oeCurrent);

        keyMapper3.setOptions(this.oa.opt.ok3, this.oa.opt.okc);
        keyMapper4.setOptions(this.oa.opt.ok4, this.oa.opt.okc);
        controller.setOptions(this.oa.opt.okc, ot());
        clock.setFrameRate(ot().frameRate);
    }

    /**
     * Set options.  Ownership is transferred.
     */
    public void setOptions(OptionsStereo os, OptionsImage oi) {
        oa.opt.os = os;
        oa.opt.oi = oi;

        engine.setOptions(oc(), ov(), os, oa.oeCurrent);
    }

    public void setEdge(int edge) {
        engine.setEdge(edge);
    }

    public void load(Store store) throws ValidationException {

        // produce a more helpful message when the file type isn't even close

        try {
            if (!store.getString(KEY_CHECK).equals(VALUE_CHECK)) throw App.getEmptyException();
        } catch (ValidationException e) {
            throw App.getException("Core.e1");
        }

        // read file, but don't modify existing objects until we're sure of success

        int dimLoad = store.getInteger(KEY_DIM);
        if (!(dimLoad == 3 || dimLoad == 4)) throw App.getException("Core.e2");

        OptionsMap omLoad = new OptionsMap(dimLoad);
        OptionsColor ocLoad = new OptionsColor();
        OptionsView ovLoad = new OptionsView();
        OptionsSeed oeLoad = new OptionsSeed();

        store.getObject(KEY_OPTIONS_MAP, omLoad);
        store.getObject(KEY_OPTIONS_COLOR, ocLoad);
        store.getObject(KEY_OPTIONS_VIEW, ovLoad);
        store.getObject(KEY_OPTIONS_SEED, oeLoad);
        if (!oeLoad.isSpecified()) throw App.getException("Core.e3");
        boolean alignModeLoad = store.getBoolean(KEY_ALIGN_MODE);

        // ok, we know enough ... even if the engine parameters turn out to be invalid,
        // we can still start a new game

        // and, we need to initialize the engine before it can validate its parameters

        dim = dimLoad;

        oa.omCurrent = omLoad; // may as well transfer as copy
        oa.ocCurrent = ocLoad;
        oa.ovCurrent = ovLoad;
        oa.oeCurrent = oeLoad;
        // oeNext is not modified by loading a game

        engine.newGame(dim, oa.omCurrent, oc(), ov(), oa.opt.os, oa.oeCurrent, false);

        controller.setOptions(oa.opt.okc, ot());
        clock.setFrameRate(ot().frameRate);

        keyMapper().releaseAll(); // sync up key mapper, which may have changed with dim
        controller.reset(dim, alignModeLoad);
        // clock will stop when controller reports idle

        // now let the engine load its parameters

        engine.load(store, alignModeLoad);
    }

    public void save(Store store) throws ValidationException {

        store.putString(KEY_CHECK, VALUE_CHECK);

        store.putInteger(KEY_DIM, dim);
        store.putObject(KEY_OPTIONS_MAP, oa.omCurrent);
        store.putObject(KEY_OPTIONS_COLOR, oc()); // ocCurrent may be null
        store.putObject(KEY_OPTIONS_VIEW, ov());  // ditto
        store.putObject(KEY_OPTIONS_SEED, oa.oeCurrent);
        store.putBoolean(KEY_ALIGN_MODE, controller.getAlignMode());

        engine.save(store);
    }

    public void keyPressed(KeyEvent e) {
        keyMapper().keyChanged(e.getKeyCode(), true);
        clock.start();
    }

    public void keyReleased(KeyEvent e) {
        keyMapper().keyChanged(e.getKeyCode(), false);
    }

    public void keyTyped(KeyEvent e) {
    }

}

