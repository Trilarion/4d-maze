/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

import org.maze4d.utils.Direction;

public class MapGeneratorBorer {

    public final int[] p;
    public int dir;

    public MapGeneratorBorer(int[] p, int dir) {
        this.p = p.clone();
        this.dir = dir;
    }

    public void move(int distance) {
        Direction.apply(dir, p, distance);
    }
}
