/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

import org.maze4d.options.OptionsMap;
import org.maze4d.utils.Direction;
import org.maze4d.utils.DynamicArray;
import org.maze4d.utils.Grid;

import java.util.LinkedList;
import java.util.Random;

/**
 * An object for generating maps according to one particular algorithm.
 */
public class MapGenerator {

    private final Map map;
    private final int[] limits;
    private final OptionsMap om;
    private final Random random;

    private final LinkedList<MapGeneratorBorer> avail; // borers
    private final LinkedList<MapGeneratorBorer> reserve;

    private final double singleBranchProbability;

    private final int[] reg1; // temporary registers
    private final int[] reg2;
    private final int[] reg3;

    public MapGenerator(Map map, int[] limits, OptionsMap om, long seed) {
        this.map = map;
        this.limits = limits;
        this.om = om;
        random = new Random(seed);

        avail = new LinkedList<>();
        reserve = new LinkedList<>();

        singleBranchProbability = computeSingleBranchProbability();

        reg1 = new int[limits.length]; // dimSpace
        reg2 = new int[limits.length];
        reg3 = new int[limits.length];

        init();
    }

// --- main loop ---

    private void init() {
        int[] start = DynamicArray.pick(limits, random);

        map.setStart(start);
        map.setOpen(start, true);

        branch(start, Direction.DIR_NONE, Direction.DIR_NONE);
        //
        // it doesn't really matter whether the new borers are available or on reserve,
        // this is just a convenient and uniform way to create them
        // the probabilities don't actually work out quite right, because of the absent directions
        //
        // (can't just start with one borer, it might immediately hit a wall and die)
    }

    public void generate() {
        int[] finish = null;

        int count = computeCount();
        while (count > 0) {
            count--;

            MapGeneratorBorer borer = pickBorer();
            if (borer == null) break; // ran out

            bore(borer);
            finish = borer.p; // OK to share
        }
        count--;

        map.setFinish(finish);
    }

// --- borer class ---

    private int computeCount() {

        // total number of interior cells
        int cells = 1;
        for (int i = 0; i < om.size.length; i++) {
            cells *= om.size[i];
        }

        // number of open cells we want
        int count = (int) Math.round(cells * om.density);

        // we've already opened the start
        count -= 1;

        // we always need a finish, regardless of density
        if (count < 1) count = 1;

        return count;
    }

// --- computations ---

    private double computeSingleBranchProbability() {

        // special case when there is no room for branches
        // (the computation below doesn't work correctly in this case)

        if (om.dimMap == 1) return 0; // value doesn't matter

        // when a borer moves, there are 2*dimMap directions in the new cell,
        // but one is already covered by the existing borer, and one is backward,
        // so there are n = 2*dimMap - 2 possible branches.

        double n = 2 * om.dimMap - 2; // use double to avoid 1/n -> 0

        // we want to find a probability for a single branch
        // so that the <i>total</i> probability of obtaining one or more branches
        // is equal to the branch probability.  it goes like this:
        //
        // branch probability = p(some branches)
        //                    = 1 - p(no branches)
        //                    = 1 - [  p(no single branch) ]^n
        //                    = 1 - [ 1 - p(single branch) ]^n
        //
        // you can also think about it in terms of risk = -ln(1-p),
        // in which case the goal is to divide the risk of branching over n directions

        return 1 - Math.pow(1 - om.branchProbability, 1 / n);
    }

    private MapGeneratorBorer pickBorer() {

        while (!avail.isEmpty()) {
            int i = random.nextInt(avail.size());
            MapGeneratorBorer borer = avail.get(i);

            if (usable(borer)) {
                return borer; // done

            } else { // borer can never become usable, remove it
                avail.remove(i);
            }
        }

        while (!reserve.isEmpty()) {
            int i = random.nextInt(reserve.size());
            MapGeneratorBorer borer = reserve.get(i);

            if (usable(borer)) {
                reserve.remove(i); // move the borer to the available list, so that we will continue to use it
                avail.add(borer);
                return borer; // done

            } else { // borer can never become usable, remove it
                reserve.remove(i);
            }
        }

        return null; // no more borers, can't reach desired density, sorry
    }

// --- helpers part 1 ---

    private void bore(MapGeneratorBorer borer) {

        // open new cell

        borer.move(1);
        map.setOpen(borer.p, true);

        // maybe turn

        int dirBackward = Direction.getOpposite(borer.dir); // save for branching

        if (om.dimMap > 1 && random.nextDouble() < om.twistProbability) {
            borer.dir = Direction.pickOrthogonal(borer.dir, om.dimMap, random);
        }

        // maybe branch

        branch(borer.p, borer.dir, dirBackward);

        // handle loops

        // the only reason we care about creating a loop
        // is that if the borer is pointing into an open cell, it will die.
        // so, in that case, maybe boost it forward one.
        // after that, it's on its own, even though it might still die

        borer.move(1);

        if (!(map.isOpen(borer.p)
                && random.nextDouble() < om.loopCrossProbability)) {
            borer.move(-1);
        }

        // note that if we move forward into an open cell,
        // that cell has already been branched, no need to do it again
    }

    /**
     * Create new borers leading off from a cell
     * in all directions except dirForward and dirBackward.
     * The branch probability is used
     * to determine whether the new borers are available.
     */
    private void branch(int[] p, int dirForward, int dirBackward) {

        for (int dir = 0; dir < 2 * om.dimMap; dir++) { // no need to consider non-map directions

            // dirForward is the direction already covered by the existing borer
            // dirBackward is already known to point to an open cell
            // (other directions might point to open cells; those borers will die when used)
            //
            if (dir == dirForward || dir == dirBackward) continue;

            MapGeneratorBorer borer = new MapGeneratorBorer(p, dir);
            if (random.nextDouble() < singleBranchProbability) {
                avail.add(borer);
            } else {
                reserve.add(borer);
            }
        }
    }

    private boolean usable(MapGeneratorBorer borer) {
        Grid.copy(reg1, borer.p);
        Direction.apply(borer.dir, reg1, 1);

        // don't go out of bounds
        if (!DynamicArray.inBounds(reg1, limits)) return false;

        // never re-open the same cell
        // (looping is when you open a cell <i>adjacent to</i> an open one)
        if (map.isOpen(reg1)) return false;

        // loop test is faster than square test, try it first
        if (!om.allowLoops && wouldMakeLoop(reg1, Direction.getOpposite(borer.dir))) return false;

        // prevent squares (and rooms), just because it is technically very convenient
        return !wouldMakeSquare(reg1);
    }

// --- helpers part 2 ---

    // use registers to avoid accidentally making a permanent change to the borer location
    // without doing a lot of memory allocation

    private boolean wouldMakeLoop(int[] base, int dirBackward) {
        Grid.copy(reg2, base);

        // it's a loop if any adjacent cell is already open
        // (not counting the one we just came from)

        for (int dir = 0; dir < 2 * om.dimMap; dir++) { // no need to consider non-map directions
            if (dir == dirBackward) continue;

            Direction.apply(dir, reg2, 1);
            if (map.isOpen(reg2)) return true;
            Direction.apply(dir, reg2, -1);
        }

        return false;
    }

    private boolean wouldMakeSquare(int[] base) {
        Grid.copy(reg2, base);

        // the plan is, look for an adjacent empty square to use as an axis,
        // then look for squares attached to that axis
        // this is a bit redundant with wouldMakeLoop, but I don't want to combine them,
        // it would be too confusing.

        for (int dir = 0; dir < 2 * om.dimMap; dir++) {

            Direction.apply(dir, reg2, 1);
            if (map.isOpen(reg2) && wouldMakeSquare(base, dir)) return true;
            Direction.apply(dir, reg2, -1);
        }

        return false;
    }

    private boolean wouldMakeSquare(int[] base, int dirAxis) {

        // optimization, only need to look for squares in higher-numbered directions

        for (int dir = dirAxis + 1; dir < 2 * om.dimMap; dir++) {
            if (Direction.isOpposite(dir, dirAxis)) continue;

            Grid.copy(reg3, base);

            Direction.apply(dir, reg3, 1);
            if (!map.isOpen(reg3)) continue;

            Direction.apply(dirAxis, reg3, 1);
            if (!map.isOpen(reg3)) continue;

            return true;
        }

        return false;
    }

}

