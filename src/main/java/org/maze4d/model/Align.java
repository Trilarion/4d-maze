/*
 * This work is dedicated to the public domain by waiving all rights to the work
 * worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.
 */

package org.maze4d.model;

import org.maze4d.utils.Direction;
import org.maze4d.utils.Grid;
import org.maze4d.utils.Vec;

/**
 * A helper object that implements alignment commands.
 */
public class Align {

    private final double[] origin; // origin and axis shared with engine
    private final double[][] axis;
    private final int dim;
    private final double[] originGoal;
    private final double[][] axisGoal;
    private final double[] reg1;
    private final double[] reg2;
    private double d;
    private double theta;
    private AlignState state;
    // --- constants ---
    private int index;

    public Align(double[] origin, double[][] axis) {

        this.origin = origin;
        this.axis = axis;
        // d and theta are set in align

        dim = origin.length;
        originGoal = new double[dim];
        axisGoal = new double[dim][dim];

        computeGoal();

        state = AlignState.TRANSLATE;
        index = 0;

        reg1 = new double[dim];
        reg2 = new double[dim];
    }

// --- external interface ---

    /**
     * Perform a single alignment step.
     *
     * @return True if the alignment is complete.
     */
    public boolean align(double d, double theta) {
        this.d = d;
        this.theta = Math.toRadians(theta);

        double f = 1;
        while (f > 0 && state != AlignState.COMPLETE) {
            f = align(f);
        }

        // I thought that doing many things in a single step
        // would look confusing, but actually it's fine.

        // another possibility is to just call align(1).
        // the problem with that is, if you're already mostly aligned,
        // you spend a few frames doing nothing.

        return (state == AlignState.COMPLETE);
    }

    /**
     * Perform some fraction of a single alignment step,
     * moving or rotating in one direction only.
     */
    private double align(double f) {
        switch (state) {

            case TRANSLATE:
                f = alignMove(index, f);
                if (f > 0) {
                    ++index;
                    if (index == dim) {
                        state = AlignState.ROTATE;
                        index = 0;
                    }
                }
                break;

            case ROTATE:
                f = alignRotate(index, f);
                if (f > 0) {
                    ++index;
                    if (index == dim) {
                        snap(); // get rid of small FP errors
                        // snap also sets STATE_COMPLETE
                    }
                }
                break;

            case COMPLETE:
                break;
        }

        return f;
    }

    /**
     * Perform the entire alignment all at once.
     */
    public void snap() {
        if (state != AlignState.COMPLETE) {
            Vec.copy(origin, originGoal);
            for (int a = 0; a < dim; a++) {
                Vec.copy(axis[a], axisGoal[a]);
            }
            state = AlignState.COMPLETE;
            index = 0;
        }
    }

// --- calculations ---

    private void computeGoal() {

        // move to cell center

        // it's unfortunate that alignment involves all this memory allocation,
        // but it's not necessary to avoid it,
        // since alignment happens on the order of once per keypress, not once per line.
        // in fact, there is probably some per-keypress allocation built into AWT

        int[] cell1 = new int[dim];
        int[] cell2 = new int[dim];
        int dir = Grid.toCell(cell1, cell2, origin);

        int[] cell = (dir != Direction.DIR_NONE && Math.random() < 0.5) ? cell2 : cell1;
        Grid.fromCell(originGoal, cell);

        // line up axes, starting with forward

        boolean[] used = new boolean[dim]; // all false to start

        for (int a = dim - 1; a >= 0; a--) {

            // first, see which real axis we are closest to
            // i.e. take dot products with real axes
            // i.e. look at coordinates of axis vector

            int iMax = -1;
            double cMax = -1; // actual c values are non-negative

            for (int i = 0; i < dim; i++) {
                if (used[i]) continue;

                double c = Math.abs(axis[a][i]);
                if (c > cMax) {
                    iMax = i;
                    cMax = c;
                }
            }

            // now point to that axis, and mark it as used

            axisGoal[a][iMax] = (axis[a][iMax] > 0) ? 1 : -1;
            // other coordinates can stay zero

            used[iMax] = true;
        }
    }

    private double alignMove(int a, double fAvailable) {
        double dAvailable = d * fAvailable;
        double dDesired = originGoal[a] - origin[a];
        double fDesired = Math.abs(dDesired) / d;

        if (fDesired > fAvailable) { // can't do in one step

            origin[a] += (dDesired > 0) ? dAvailable : -dAvailable;
            fAvailable = 0;

        } else {

            origin[a] = originGoal[a]; // avoid FP error (but not necessary, see note below)
            fAvailable -= fDesired;
        }

        return fAvailable;
    }

    private double alignRotate(int a, double fAvailable) {
        double thetaAvailable = theta * fAvailable;

        double dot = Vec.dot(axisGoal[a], axis[a]);
        if (dot > 1) dot = 1;

        // dot > 1 could happen because of accumulated FP error,
        // and that would cause acos to return NaN
        //
        // dot < -1 can't happen ... we get to choose the sign of the goal axes,
        // so thetaDesired is never more than 90 degrees.

        double thetaDesired = Math.acos(dot);
        double fDesired = thetaDesired / theta;

        if (fDesired > fAvailable) { // can't do in one step

            rotate(a, dot, thetaAvailable);
            fAvailable = 0;

        } else {

            rotate(a, dot, thetaDesired);
            fAvailable -= fDesired;

            // you might think we'd want to snap the axis now to avoid FP error,
            // but it would get rotated some more later anyway.  we will snap at the end.
        }

        return fAvailable;
    }

    /**
     * Imagine the rotation that moves axis[a] in the direction of axisGoal[a] by theta <i>radians</i>,
     * and apply it to all the axes.
     */
    private void rotate(int a, double dot, double theta) {
        if (theta == 0) return;
        // implies that axis and axisGoal are the same, in which case there's no orthogonal vector

        // copy axis[a] into reg1 so that we don't have to worry about changing axis[a]
        Vec.copy(reg1, axis[a]);

        // copy axisGoal[a] into reg2, and make it orthogonal to reg1
        Vec.addScaled(reg2, axisGoal[a], reg1, -dot);
        Vec.normalize(reg2, reg2);

        double cos = Math.cos(theta);
        double sin = Math.sin(theta);

        for (int i = 0; i < dim; i++) {

            // compute components in reg1-reg2 plane
            double x1 = (i == a) ? 1 : 0; // axes are orthonormal
            double x2 = Vec.dot(axis[i], reg2);

            // so, we have  axis[i] = something + x1*     reg1             + x2*     reg2
            // and we want to compute something + x1*(cos*reg1 + sin*reg2) + x2*(cos*reg2 - sin*reg1)

            Vec.addScaled(axis[i], axis[i], reg1, x1 * (cos - 1) - x2 * sin);
            Vec.addScaled(axis[i], axis[i], reg2, x2 * (cos - 1) + x1 * sin);
        }
    }

}

