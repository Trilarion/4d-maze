.. 4D Maze Game documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

4D Maze Game's documentation
============================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   content


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
