############
4D Maze Game
############

Welcome to the four-dimensional maze game! And yes, that's right … the game is quite literally set
in a four-dimensional world. A typical passage looks something like this, only larger, because you
can use the whole screen, and better, because you can adjust the 3D effect to fit your own eyes.

.. figure:: /images/front.gif

   To view, cross your eyes until the cubes line up, then wait until your eyes refocus.
   
The game is free (source code is available), and lots of fun!

What's in all this documentation, anyway? Well, first of all, if you're still wondering how a
four-dimensional game is even possible, there's a short page that explains the idea. Then there's
a whole section about things you can see in the maze, along with some notes about various aspects
of the four-dimensional world. Finally, just in case the program isn't self-explanatory, there's a
reference manual.

One of the best ways to understand the four-dimensional world is to compare it to an analogous
three-dimensional world. To that end, the maze game can also be played in three-dimensional mode.

The game has some other notable features, too. It generates a variety of good random mazes. It has
lots of options, including several that control how the maze is displayed. It starts in align mode,
so you don't get completely disoriented, but allows you to fly around freely if you want.

The game was originally created by `John McIntosh <http://www.urticator.net/maze/>`_ in 2002 to 2008
and slightly adapted in 2021.

********
Download
********



********
The Idea
********

How is a four-dimensional maze possible?
========================================

The actual world we live in has three dimensions of space plus one of time, so people often refer to
time as the fourth dimension, but I am not talking about a maze that extends over time. In fact, I am
not even talking about the actual world we live in! I am imagining a different world, one that has four
dimensions of space (plus one of time). In that world, you can build four-dimensional mazes, just as
you can build three-dimensional ones here.

How can you see a four-dimensional maze?
========================================

As a three-dimensional person, you see the world by having it projected onto a two-dimensional retina,
i.e., by receiving a two-dimensional set of colors. Similarly, a four-dimensional person would see the
world by having it projected onto a three-dimensional retina.

It is difficult to display a three-dimensional set of colors on a two-dimensional computer screen, so
I decided to consider black to be transparent and arrange for most of the world to be black. That reduced
the original solid block of color to a set of three-dimensional lines. Then I created a three-dimensional
vector display using stereo pairs of images.

Thus, it is possible to observe the four-dimensional maze world exactly as if you had one four-dimensional eye.

***********
In the Maze
***********

In the following pages, I'll be taking you on a guided tour of the four-dimensional maze world, showing
you some of its more common features and comparing them to the analogous three-dimensional ones.

If you like figuring things out for yourself, stop! Everything I'm going to say here can be discovered
by playing the game and thinking about the analogy. You can always come back and read this later.

Forward Wall
============

Before we start looking around at the maze world, let's take a moment and notice that there are a couple
of features that are actually part of the eye.

.. image:: /images/wf-1a.gif
.. image:: /images/wf-1b.gif

The cube (or square) marks the boundary of the retina, beyond which we can't see anything, while the
crosshairs indicate which way we're looking.

Now let's look at the simplest possible thing, a single wall right in front of us.

.. image:: /images/wf-2a.gif
.. image:: /images/wf-2b.gif

That's not much to look at, is it? Fortunately, we can easily change the wall's appearance using the
view options. The picture above was made with only boundaries turned on; if we also turn on all the
textures, we get the following.

.. image:: /images/wf-3a.gif
.. image:: /images/wf-3b.gif

That's better, but it will be a bit much when we start adding other walls to the picture. So, for the
rest of the tour, we will use only texture number 7, like so.

.. image:: /images/wf-4a.gif
.. image:: /images/wf-4b.gif

The default within the game, by the way, is to use only texture number 8, but with boundaries turned off.

Side Walls
==========

So far, all we have is a single wall right in front of us.

.. image:: /images/wf-4a.gif
.. image:: /images/wf-4b.gif

Let's add some side walls. How about a purple wall on our right …

.. image:: /images/ws-1a.gif
.. image:: /images/ws-1b.gif

… and a green wall on our left?

.. image:: /images/ws-2a.gif
.. image:: /images/ws-2b.gif

We can also add a floor and ceiling.

.. image:: /images/ws-3a.gif
.. image:: /images/ws-3b.gif

In three dimensions, that's all we can do, but in four dimensions we can also add walls in the in and out directions.

.. image:: /images/blank.gif
.. image:: /images/ws-4b.gif

The wall in the in direction is yellow, the one in the out direction is red.

Just for completeness, let's add one more wall, right behind us, so that we're completely enclosed.
We'll have to imagine that one, since we can't see it.

When you're looking at a picture like that within the game, on a full screen and in stereo, it's easy
to figure out which lines are which. Here, however, I think the lines are already a little too dense—and
soon there are going to be even more of them! So, to keep things simple, let's turn off textures on the
floor and ceiling and on the walls in the in and out directions, like so.

.. image:: /images/ws-2a.gif
.. image:: /images/ws-2b.gif

That's not something you can do within the game, by the way.

Side Passages
=============

So here we are, completely enclosed in a single square of the maze.

.. image:: /images/ws-2a.gif
.. image:: /images/ws-2b.gif

Let's create a passage leading to the right. The right wall goes away, and in its place we see
the back wall of the next square over (plus some other walls that don't have textures).

.. image:: /images/ps-1a.gif
.. image:: /images/ps-1b.gif

Similarly, a passage leading upward looks like this.

.. image:: /images/ps-2a.gif
.. image:: /images/ps-2b.gif

There can also be passages leading left and downward, but let's skip those and look at a passage
that leads … inward! We can only do that in four dimensions, of course.

.. image:: /images/blank.gif
.. image:: /images/ps-3b.gif

As three-dimensional people, it's easy for us to confuse inward with forward, but they really aren't
he same thing. The original wall is still right there in front of us.