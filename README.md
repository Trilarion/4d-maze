# 4D Maze

4D Maze visualizes and allows to navigate in a four-dimensional maze. It is based on an earlier version
published in 2008 (see [history](HISTORY.md)) by John McIntosh.

It is written in Java. This work is dedicated to the public domain (see [license](LICENSE)) by waiving all rights to the work
worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.

Currently, there is no official release.