# 4D Maze Game

## Development 2002 - 2008

4D Maze was initially developed by John McIntosh at http://www.urticator.net/maze/
to visualize and navigate in a four-dimensional maze. It was written in Java and
released into the Public domain.

## Continuation in 2021

Even though the binary release from 2009 was still runnable in 2021 on an JRE, the project was continued
on [Gitlab](https://gitlab.com/Trilarion/4d-maze) by Trilarion. The aim was to adapt the code base to recent Java language developments,
streamline it and improve the app with a reasonable effort, while keeping the game mechanics exactly equal.